import {
  config
} from '../../../utils/config.js'
import {
  HTTP
} from '../../../utils/http.js';
const http = new HTTP();
Page({
  /**
  * delBtnWidth: 删除按钮的宽度单位
  * list: 循环的mock数据
  * startX: 收支触摸开始滑动的位置
  */
  data: {
    delBtnWidth: 180,
    list: [],
    startX: "",
    noMem: false,//搜索结果是否为空
  },
  onLoad: function (options) {
    
  },
  onShow: function () {
    let params = {
      url: config.getblacklist,
      data: {
        token: wx.getStorageSync('token')
      }
    }
    http.request(params, (res) => {
      if(!res.data.list[0]) {
        wx.showToast({
          title: '暂无拒收用户',
          icon: 'none'
        })
      }
      this.setData({
        list: res.data.list
      })
    })
  },

  // 查看所有拒收用户
  getAllmem () {
    this.onShow()
  },

  // 获取搜索的快递员
  getMem (e) {
    let value = e.detail.value;
    this.setData({
      searchValue: value
    })
  },

  // 搜索
  searchMem (e) {
    wx.showLoading({
    })
    let value = this.data.searchValue;
    let searchList = [];
    let allList = this.data.list;
    for (let i = 0; i < allList.length; i ++) {
      if (allList[i].phone == value) {
        searchList.push(allList[i])
      }
    }
    if (!searchList[0]) {
      this.onShow()
      wx.showToast({
        title: '未找到相关用户',
        icon: 'none',
        duration: 2000
      })
    }
    else {
      this.setData({
        noMem: false,
        list: searchList
      })
    }
    setTimeout(function() {
      wx.hideLoading()
    },500)
  },

  touchS: function (e) {
    if (e.touches.length == 1) {
      this.setData({
        //设置触摸起始点水平方向位置
        startX: e.touches[0].clientX
      });
    }
  },
  touchM: function (e) {
    if (e.touches.length == 1) {
      //手指移动时水平方向位置
      var moveX = e.touches[0].clientX;
      //手指起始点位置与移动期间的差值
      var disX = this.data.startX - moveX;
      var delBtnWidth = this.data.delBtnWidth;
      var txtStyle = "";
      if (disX == 0 || disX < 0) {//如果移动距离小于等于0，说明向右滑动，文本层位置不变
        txtStyle = "left:0px";
      } else if (disX > 0) {//移动距离大于0，文本层left值等于手指移动距离
        txtStyle = "left:-" + disX + "px";
        if (disX >= delBtnWidth) {
          //控制手指移动距离最大值为删除按钮的宽度
          txtStyle = "left:-" + delBtnWidth + "px";
        }
      }
      //获取手指触摸的是哪一项
      var index = e.currentTarget.dataset.index;
      var list = this.data.list;
      list[index].txtStyle = txtStyle;
      //更新列表的状态
      this.setData({
        list: list
      });
    }
  },
  touchE: function (e) {
    if (e.changedTouches.length == 1) {
      //手指移动结束后水平位置
      var endX = e.changedTouches[0].clientX;
      //触摸开始与结束，手指移动的距离
      var disX = this.data.startX - endX;
      var delBtnWidth = this.data.delBtnWidth;
      //如果距离小于删除按钮的1/2，不显示删除按钮
      var txtStyle = disX > delBtnWidth / 2 ? "left:-" + delBtnWidth + "rpx" : "left:0px";
      //获取手指触摸的是哪一项
      var index = e.currentTarget.dataset.index;
      var list = this.data.list;
      list[index].txtStyle = txtStyle;
      //更新列表的状态
      this.setData({
        list: list
      });
    }
  },

  //点击删除按钮事件
  delItem: function (e) {
    //获取列表中要删除项的下标
    let index = e.currentTarget.dataset.index
    // 获取手机号
    let phone = e.currentTarget.dataset.phone;
    let list = this.data.list;
    let params = {
      url: config.cancelblacklist,
      data: {
        token: wx.getStorageSync('token'),
        phone: phone
      }
    }
    http.request(params,(res)=> {
      wx.showToast({
        title: '取消成功',
      })
      //移除列表中下标为index的项
      list.splice(index, 1);
      //更新列表的状态
      this.setData({
        list: list
      });
    })
    
  },

  // 跳转添加拒收用户
  addUser () {
    wx.navigateTo({
      url: '/pages/rejectManage/addUser/addUser',
    })
  }
})