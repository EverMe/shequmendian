// pages/rejectManage/addUser/addUser.js
import {
  config
} from '../../../utils/config.js'
import {
  HTTP
} from '../../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    phone: '',
    note: '',
    noteLength: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  // 监听备注信息
  noteChange (e) {
    let value = e.detail.value;
    this.setData({
      note: value,
      noteLength: value.length
    })
  },

  // 获取手机号
  getPhone (e) {
    let value = e.detail.value;
    this.setData({
      phone: value
    })
  },

  // 重置
  clearInput () {
    this.setData({
      phone: '',
      note: '',
    })
  },

  // 绑定
  binding () {
    if(!this.data.phone) {
      wx.showToast({
        title: '请输入手机号',
        icon: 'none'
      })
      return;
    }
    else if (this.data.phone.length != 11) {
      wx.showToast({
        title: '手机号格式不正确',
        icon: 'none'
      })
      return;
    }
    let params = {
      url: config.setblacklist,
      data: {
        token: wx.getStorageSync('token'),
        phone: this.data.phone,
        note: this.data.note
      }
    }
    http.request(params,(res)=> {
      wx.showToast({
        title: '绑定成功',
        mask: true
      })
      setTimeout(()=> {
        wx.navigateBack({
          
        })
      },1500)
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})