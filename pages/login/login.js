// pages/login/login.js
import {
  config
} from '../../utils/config.js'
import {
  HTTP
} from '../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  // 登录事件
  login: function(e) {
    wx.showLoading({
      title: '登录中',
    })
    let value = e.detail.value;
    let passwordRe = /^[a-z0-9_-]{6,18}$/;
    if (!value.phone) {
      wx.showToast({
        title: '请输入手机号',
        icon: 'none'
      })
    } else if (value.phone.length < 11) {
      wx.showToast({
        title: '手机号不是11位，请检查！',
        icon: 'none'
      })
    } else if (!value.password) {
      wx.showToast({
        title: '请输入密码',
        icon: 'none'
      })
    } else if (!passwordRe.test(value.password)) {
      wx.showToast({
        title: '请输入6-18位密码',
        icon: 'none'
      })
    } else {
      wx.login({
        success: res => {
          if (res.code) {
            let params = {
              url: config.loginv2,
              data: {
                username: value.phone,
                password: value.password,
                jsCode: res.code
              }
            }
            http.request(params, (res) => {
              wx.hideLoading();
              let info = res.data
                wx.setStorageSync('info', info)
                wx.setStorageSync('token', info.token)
                wx.switchTab({
                  url: '/pages/home/home',
                  success: function (e) {
                    wx.showToast({
                      title: '登录成功',
                      duration: 2000
                    })
                    var page = getCurrentPages().pop();
                    if (page == undefined || page == null) return;
                    page.onLoad();
                  }
                })
            })
          } else {
            wx.showModal({
              content: '获取用户登录态失败！' + res.errMsg
            })
          }
        }
      });
      
    }
  },

  // 跳转注册
  toRegist: function() {
    wx.navigateTo({
      url: "/pages/Reg/Reg"
    })
  },

})