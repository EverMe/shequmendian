// pages/searchWaybill/waybill.js
import {
  config
} from '../../utils/config.js'
import {
  HTTP
} from '../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    reset:false,
    codeValue:'',
    focus:false,
    noWaybill:true,
    searchPage:1,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let codeValue = options.codeValue;
    this.setData({
      codeValue: codeValue,
      reset: true
    })
    this.searchWaybill()
  },

  // 去详情页
  toDetail: function (e) {
    let detail = e.currentTarget.dataset.item
    console.log(e.currentTarget.dataset.item)
    wx.navigateTo({
      url: '/pages/waybillDetail/detail?detail=' + JSON.stringify(detail),
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  // 扫码
  scanCode: function() {
    var that = this;
    wx.scanCode({
      success:(res) => {
        this.setData({
          searchText:true,
          codeValue: res.result,
          reset: true,
          waybillValue: res.result,
        })
        that.searchWaybill()
      }
    })  
  },

  // 监听输入框
  searchValue: function(e) {
    this.setData({
      codeValue:e.detail.value
    })
    if(!this.data.reset) {
      this.setData({
        reset: true,
        searchText: true,
      })
    }
    else if(e.detail.value == '') {
      this.setData({
        reset: false,
        searchText: false,
      })
    }
  },

  // 重置输入框内容
  reset: function() {
    console.log('清除')
    this.setData({
      codeValue:'',
      reset: false,
      focus:true
    })
  },

// 搜索请求
  searchWaybill: function() {
    let params = {
      url: config.searchcabinetlist,
      data:{
        token: wx.getStorageSync('token'),
        keyWord: this.data.codeValue,
        page:1,
        size:10,
      }
      
    }
    http.request(params,(data) => {
      this.setData({
        waybillList:data.data.list,
        noWaybill: true,
      })
      if(!data.data.list[0]) {
        this.setData({
          noWaybill:false
        })
      }
    })
  },

 

  // 6931950703724

  loadMore:function(){
    wx.showLoading({
      title:'加载中',
      mask:true
    })
    this.data.searchPage++;
    let params = {
      url: config.searchcabinetlist,
      data: {
        token: wx.getStorageSync('token'),
        keyWord: this.data.codeValue,
        page: this.data.searchPage,
        size: 10,
      }

    }
    http.request(params, (data) => {
      wx.hideLoading()
      if (!data.data.list[0]) {
        wx.showToast({
          title: '暂无更多订单了',
          icon: 'none'
        })
      }
      for(let i = 0;i < data.data.list.length;i++) {
        this.data.waybillList.push(data.data.list[i])
      }
      this.setData({
        waybillList: this.data.waybillList,
        noWaybill: true,
      })
    })
  }
})