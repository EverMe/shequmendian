var app = getApp();
Page({
  /**
  * delBtnWidth: 删除按钮的宽度单位
  * list: 循环的mock数据
  * startX: 收支触摸开始滑动的位置
  */
  data: {
    delBtnWidth: 180,
    list: [
      {
        txtStyle: "",
        icon: "/images/icon0.png",
        txt: "向左滑动可以删除"
      },
      {
        txtStyle: "",
        icon: "/images/icon6.png",
        txt: "微信小程序|联盟（wxapp-union.com）"
      },
      {
        txtStyle: "",
        icon: "/images/icon1.png",
        txt: "圣诞老人是爸爸，顺着烟囱往下爬，礼物塞满圣诞袜，平安糖果一大把"
      },
      {
        txtStyle: "",
        icon: "/images/icon2.png",
        txt: "圣诞到来，元旦还会远吗？在圣诞这个日子里"
      },
      {
        txtStyle: "",
        icon: "/images/icon3.png",
        txt: "圣诞节(Christmas或Cristo Messa ),译名为“基督弥撒”。"
      },
      {
        txtStyle: "",
        icon: "/images/icon4.png",
        txt: "一年一度的圣诞节即将到来,姑娘们也纷纷开始跑趴了吧!"
      },

      {
        txtStyle: "",
        icon: "/images/icon5.png",
        txt: "圣诞节(Christmas或Cristo Messa ),译名为“基督弥撒”。"
      },
      {
        txtStyle: "",
        icon: "/images/icon2.png",
        txt: "你的圣诞节礼物准备好了吗?"
      },
      {
        txtStyle: "",
        icon: "/images/icon3.png",
        txt: "一年一度的圣诞节即将到来,姑娘们也纷纷开始跑趴了吧!"
      },
      {
        txtStyle: "",
        icon: "/images/icon4.png",
        txt: "圣诞到来，元旦还会远吗？"
      },
      {
        txtStyle: "",
        icon: "/images/icon5.png",
        txt: "记下这一刻的心情"
      },
    ],
    startX: ""
  },
  onLoad: function (options) {
  },
  touchS: function (e) {
    if (e.touches.length == 1) {
      this.setData({
        //设置触摸起始点水平方向位置
        startX: e.touches[0].clientX
      });
    }
  },
  touchM: function (e) {
    if (e.touches.length == 1) {
      //手指移动时水平方向位置
      var moveX = e.touches[0].clientX;
      //手指起始点位置与移动期间的差值
      var disX = this.data.startX - moveX;
      var delBtnWidth = this.data.delBtnWidth;
      var txtStyle = "";
      if (disX == 0 || disX < 0) {//如果移动距离小于等于0，说明向右滑动，文本层位置不变
        txtStyle = "left:0px";
      } else if (disX > 0) {//移动距离大于0，文本层left值等于手指移动距离
        txtStyle = "left:-" + disX + "px";
        if (disX >= delBtnWidth) {
          //控制手指移动距离最大值为删除按钮的宽度
          txtStyle = "left:-" + delBtnWidth + "px";
        }
      }
      //获取手指触摸的是哪一项
      var index = e.currentTarget.dataset.index;
      var list = this.data.list;
      list[index].txtStyle = txtStyle;
      //更新列表的状态
      this.setData({
        list: list
      });
    }
  },
  touchE: function (e) {
    if (e.changedTouches.length == 1) {
      //手指移动结束后水平位置
      var endX = e.changedTouches[0].clientX;
      //触摸开始与结束，手指移动的距离
      var disX = this.data.startX - endX;
      var delBtnWidth = this.data.delBtnWidth;
      //如果距离小于删除按钮的1/2，不显示删除按钮
      var txtStyle = disX > delBtnWidth / 2 ? "left:-" + delBtnWidth + "px" : "left:0px";
      //获取手指触摸的是哪一项
      var index = e.currentTarget.dataset.index;
      var list = this.data.list;
      list[index].txtStyle = txtStyle;
      //更新列表的状态
      this.setData({
        list: list
      });
    }
  },
  
  //点击删除按钮事件
  delItem: function (e) {
    //获取列表中要删除项的下标
    var index = e.currentTarget.dataset.index;
    var list = this.data.list;
    //移除列表中下标为index的项
    list.splice(index, 1);
    //更新列表的状态
    this.setData({
      list: list
    });
  },
})