// pages/sms/buyRecord/record.js
import {
  config
} from '../../../utils/config.js'
import {
  HTTP
} from '../../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    buyList: [],
    page: 1,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getRecord()
  },

  // 获取记录
  getRecord () {
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    let params = {
      url: config.costhistory,
      data: {
        token: wx.getStorageSync('token'),
        page: this.data.page,
        limit: 15
      }
    }
    http.request(params, (res) => {
      wx.hideLoading()
      let data = res.data.list;
      if(!data[0]) {
        wx.showToast({
          title: '没有更多记录了',
          icon: 'none'
        })
      }
      let buyList = this.data.buyList
      for(let i = 0; i < data.length; i ++) {
        buyList.push(data[i])
      }
      this.setData({
        buyList: buyList
      })
    })
  },

  // 下拉加载
  loadMore () {
    this.setData({
      page: this.data.page + 1
    })
    this.getRecord()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})