// pages/sms/sendRecord/record.js
import {
  config
} from '../../../utils/config.js'
import {
  HTTP
} from '../../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    msmList: [],
    phone: '',
    page: 1,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getRecord()
  },

  getRecord () {
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    let params = {
      url: config.usedhistory,
      data: {
        token: wx.getStorageSync('token'),
        phone: this.data.phone,
        page: this.data.page,
        limit: 15
      }
    }
    http.request(params, (res) => {
      wx.hideLoading()
      let data = res.data.list;
      if (!data[0]) {
        wx.showToast({
          title: '未找到相关记录',
          icon: 'none'
        })
      }
      let msmList = this.data.msmList;
      for (let i = 0; i < data.length; i++) {
        msmList.push(data[i])
      }
      this.setData({
        msmList: msmList
      })
    })
  },

  // 加载更多
  loadMore () {
    this.setData({
      page: this.data.page + 1
    })
    this.getRecord()
  },

  // 监听搜索框
  valueChange (e) {
    let value = e.detail.value;
    this.setData({
      phone: value
    })
  },

  // 查询记录
  searchRecord () {
    this.setData({
      msmList: [],
      page: 1
    })
    this.getRecord()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})