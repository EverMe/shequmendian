// pages/sms/home/home.js
import {
  config
} from '../../../utils/config.js'
import {
  HTTP
} from '../../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    viewsetmeal: [],
    selectCombo: "",//选中套餐的index
    selectVallet: false,//是否选中钱包支付
    selectWechatPay: false,//是否选中微信支付
    loading: false,//按钮加载状态
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取短信包套餐
    let params = {
      url: config.viewsetmeal,
      data: {
        token: wx.getStorageSync('token')
      }
    }
    http.request(params,(res)=> {
      this.setData({
        viewsetmeal: res.data.list
      })
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 获取短信条数及余额
    let params = {
      url: config.getinfo,
      data: {
        token: wx.getStorageSync('token')
      }
    }
    http.request(params,(res)=> {
      this.setData({
        amount: res.data.info
      })
    })
  },

  // 选中套餐
  selectTap (e) {
    let index = e.currentTarget.dataset.index;
    let productId = e.currentTarget.dataset.productid;
    this.setData({
      selectCombo: index,
      productId: productId
    })
  },

  // 支付
  payfor () {
    if ((this.data.selectCombo !== '') && this.data.selectVallet) {
      this.setData ({
        loading: true
      })
      let params = {
        url: config.costbyaccount,
        data: {
          token: wx.getStorageSync('token'),
          productId: this.data.productId
        }
      }
      http.request(params,(res)=> {
        wx.navigateTo({
          url: '/pages/paySuccess/paySuccess',
        })
        this.setData({
          selectCombo: "",//选中套餐的index
          selectVallet: false,//是否选中钱包支付
          selectWechatPay: false,//是否选中微信支付
          loading: false,//按钮加载状态
        })
      })
      setTimeout(()=> {
        this.setData({
          loading: false
        })
      }, 2000)
    } 
  },


  // 选中钱包支付
  selectVallet () {
    if (this.data.selectWechatPay) {
      this.setData({
        selectWechatPay: false
      })
    }
    this.setData({
      selectVallet: !this.data.selectVallet
    })
  },

  // 选中微信支付
  selectWechatPay() {
    if (this.data.selectVallet) {
      this.setData({
        selectVallet: false
      })
    }
    this.setData({
      selectWechatPay: !this.data.selectWechatPay
    })
  },

  // 购买记录
  buyRecord () {
    wx.navigateTo({
      url: '/pages/sms/buyRecord/record',
    })
  },

  // 发送记录
  sendRecord() {
    wx.navigateTo({
      url: '/pages/sms/sendRecord/record',
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})