// pages/mouthManage/mouthManage.js
import {
  config
} from '../../utils/config.js'
import {
  HTTP
} from '../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    startDate: "",//开始时间
    endDate:"",//结束时间
    comState:false,//快递公司下拉菜单
    comName:'全部',//选中的快递公司
    expressCabinet: "",//柜机数据
    expressComList: [{comId: '',comName: '全部'}],//快递公司数据
    expressRecords: [],//格口记录数据
    cabId:"",//柜机id
    boxCode: "",
    startTimestamp: '',
    endTimestamp: '',
    comId: '',//快递公司ID
    page: 1,//默认页数
    limit: 10,//默认
    hideSelectDate: true,//是否隐藏选择时间
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取当天日期
    let nowDate = this.timestampToDate(Date.parse(new Date()))
    let nowDateCN = this.timestampToDateCN(Date.parse(new Date()))
    this.setData({
      startDate: nowDateCN,
      endDate: nowDateCN
    })
    
    // 结束时间戳
    let endTimestamp = Date.parse(nowDate) + (3600 * 24 - 1) * 1000
    // 开始时间戳
    let startTimestamp = Date.parse(nowDate) - (3600 * 24) * 1000 * 7
    this.setData({
      startTimestamp: startTimestamp,
      endTimestamp: endTimestamp
    })
    let searchData = JSON.parse(options.searchData)
    this.setData({
      cabId: searchData.cabId,
      searchData: searchData
    })
    this.getRecoed()
  },

  // 获取记录的方法
  getRecoed () {
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    let params = {
      url: config.getrecords,
      data: {
        token: wx.getStorageSync('token'),
        cabId: this.data.searchData.cabId,
        boxNo: this.data.searchData.boxNum,
        boxCode: this.data.searchData.boxSite,
        column: this.data.searchData.column && +this.data.searchData.column ,
        row: this.data.searchData.row && + this.data.searchData.row,
        startTime: this.data.startTimestamp / 1000,
        // startTime: null,
        endTime: this.data.endTimestamp / 1000,
        // endTime: null,
        comId: this.data.comId,
        page: this.data.page,
        limit: 10
      }
    };
    http.request(params, (res) => {
      wx.hideLoading()
      let data = res.data
      if(this.data.page === 1) {
        if (!data.expressRecords[0]) {
          wx.showToast({
            title: '暂无记录',
            icon: 'none'
          })
        }
      } else if (!data.expressRecords[0]) {
        wx.showToast({
          title: '没有更多记录了',
          icon: 'none'
        })
        // 没有记录，页数返回
        this.setData({
          page: this.data.page - 1
        })
        return;
      }
      for (let i = 0; i < data.expressRecords.length; i++) {
        data.expressRecords[i]['hidden'] = false;
        this.data.expressRecords.push(data.expressRecords[i]);
      }
      this.data.expressComList = [{ comId: '', comName: '全部' }]
      this.setData({
        boxCode: data.expressCabinet.boxCode,
        expressCabinet: data.expressCabinet,
        expressComList: this.data.expressComList.concat(data.coms),
        expressRecords: this.data.expressRecords
      })
    })
  },

  // 开关
  switchTap (e) {
    let index = e.currentTarget.dataset.index;
    let expressRecords = this.data.expressRecords;
    expressRecords[index].hidden = !expressRecords[index].hidden;
    this.setData({
      expressRecords: expressRecords
    })
  },

  // 选择快递公司
  comSelect (e) {
    let value = e.detail.value;
    let expressComList = this.data.expressComList;
    this.setData({
      comName: expressComList[value].comName,
      comId: expressComList[value].comId,
      expressRecords: []
    })
    this.getRecoed()
  },

  // 时间戳转化为年月日方法
  timestampToDate(timestamp) {
    var date = new Date(timestamp); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
    var Y = date.getFullYear();
    var M =
      date.getMonth() + 1 < 10
        ? "0" + (date.getMonth() + 1)
        : date.getMonth() + 1;
    var D = date.getDate() + " ";
    var h = date.getHours() + ":";
    var m = date.getMinutes() + ":";
    var s = date.getSeconds();
    return `${Y}/${M}/${D}`;
  },

  // 时间戳转化为年月日方法(中文)
  timestampToDateCN(timestamp) {
    var date = new Date(timestamp); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
    var Y = date.getFullYear();
    var M =
      date.getMonth() + 1 < 10
        ? "0" + (date.getMonth() + 1)
        : date.getMonth() + 1;
    var D = date.getDate() + " ";
    var h = date.getHours() + ":";
    var m = date.getMinutes() + ":";
    var s = date.getSeconds();
    return `${Y}年${M}月${D}日`;
  },

  // 选择开始时间
  bindStartDateChange: function (e) {
    let date = e.detail.value
    date = date.replace(/'-'/g, '/')
    let dateCN = this.timestampToDateCN(Date.parse(date))
    this.setData({
      startDate: dateCN,
      startTimestamp: Date.parse(date)
    })
  },

  // 选择结束时间
  bindEndDateChange: function (e) {
    let endDate = e.detail.value
    endDate = endDate.replace(/'-'/g,'/')
    let endDateCN = this.timestampToDateCN(Date.parse(endDate))
    this.setData({
      endDate: endDateCN,
      endTimestamp: (Date.parse(endDate) + (3600 * 24 - 1) * 1000)
    })
  },

  // 打开选择时间
  openSelectDate () {
    this.setData({
      hideSelectDate: !this.data.hideSelectDate
    })
  },

  closeModel () {
    this.setData({
      hideSelectDate: true
    })
  },

  // 筛选
  selectRecord () {
    if(this.data.startTimestamp > this.data.endTimestamp) {
      wx.showToast({
        title: '开始时间不能大于结束时间，请重新选择',
        icon: 'none',
        duration: 2000
      })
    }
    else {
      this.setData({
        page: 1,
        expressRecords: [],
        hideSelectDate: true
      })
      this.getRecoed()
    }
  },

  // 下拉加载更多数据
  loadMore () {
    this.setData({
      page: this.data.page + 1
    })
    this.getRecoed()
  },

  // 开柜
  openone: function() {
    let that = this;
    wx.showModal({
      title: '提示',
      content: '确定开柜吗？',
      success(res) {
        if (res.confirm) {
          wx.showLoading({
            title: '请稍后',
            mask: true
          })
          let params = {
            url: config.sendcommond,
            data: {
              token: wx.getStorageSync('token'),
              cabId: that.data.cabId,
              boxCode: that.data.boxCode,
              cmd: 'OPENONE'
            }
          }
          http.request(params, (res) => {
            wx.hideLoading()
            wx.showToast({
              title: '开柜成功',
            })
          })
        } else if (res.cancel) {
        }
      }
    })
    
  },

  // 锁柜
  lock: function () {
    let that = this;
    wx.showModal({
      title: '提示',
      content: '确定锁柜吗？',
      success(res) {
        if (res.confirm) {
          wx.showLoading({
            title: '请稍后',
            mask: true
          })
          let params = {
            url: config.sendcommond,
            data: {
              token: wx.getStorageSync('token'),
              cabId: that.data.cabId,
              boxCode: that.data.boxCode,
              cmd: 'LOCK'
            }
          }
          http.request(params, (res) => {
            wx.hideLoading()
            wx.showToast({
              title: '锁柜成功',
            })
          })
        } else if (res.cancel) {
        }
      }
    })
  },

  // 解锁
  unlock: function () {
    let that = this;
    wx.showModal({
      title: '提示',
      content: '确定解锁吗？',
      success(res) {
        if (res.confirm) {
          wx.showLoading({
            title: '请稍后',
            mask: true
          })
          let params = {
            url: config.sendcommond,
            data: {
              token: wx.getStorageSync('token'),
              cabId: that.data.cabId,
              boxCode: that.data.boxCode,
              cmd: 'UNLOCK'
            }
          }
          http.request(params, (res) => {
            wx.hideLoading()
            wx.showToast({
              title: '解锁成功',
            })
          })
        } else if (res.cancel) {
        }
      }
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})