// pages/vallet/bankCard/card.js
import {
  config
} from '../../../utils/config.js'
import {
  HTTP
} from '../../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    codeValue: '发送验证码',
    codeDisable: false, //是否禁用发送验证码
    bankName: '',
    bankAddress: '',
    bankCard: '',
    bindCode: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    // if (options) {
    //   let cardInfo = JSON.parse(options.cardInfo)
    //   this.setData({
    //     bankName: cardInfo.bankName,
    //     bankCard: cardInfo.bankCard,
    //     bindCode: this.data.bindCode
    //   })
    // }
  },
  // 发送验证码
  sendCode() {
    let countDown = 60;
    if (this.data.codeValue != '发送验证码') {
      return;
    }
    let params = {
      url: config.sendmsg,
      data: {
        token: wx.getStorageSync('token'),
        type: 2
      }
    }
    http.request(params, (res) => {
      wx.showToast({
        title: '验证码已发送',
        icon: 'none'
      })
    })
    this.setData({
      codeValue: countDown
    })
    this.wite(countDown)
  },

  wite(countDown) {
    this.setData({
      codeValue: countDown + 's'
    })
    countDown -= 1;
    if (countDown > -1) {
      setTimeout(()=>{
        this.wite(countDown)
      },1000)
    } else {
      this.setData({
        codeValue: '发送验证码'
      })
    }
  },

  // 设置银行卡
  setbankCard () {
    if(!this.data.bankName) {
      wx.showToast({
        title: '请输入开户银行',
        icon: 'none'
      })
      return;
    }

    if (!this.data.bankAddress) {
      wx.showToast({
        title: '请输入开户支行',
        icon: 'none'
      })
      return;
    }

    if (!this.data.bankCard) {
      wx.showToast({
        title: '请输入银行卡号',
        icon: 'none'
      })
      return;
    }
    let params = {
      url: config.setbankcardinfo,
      data: {
        token: wx.getStorageSync('token'),
        bankName: this.data.bankName,
        bankCard: this.data.bankCard,
        bindCode: this.data.bindCode,
        bankAddress: this.data.bankAddress
      }
    }
    http.request(params,(res)=> {
      wx.showToast({
        title: '设置成功',
        mask: true
      })
      setTimeout((res)=> {
        wx.navigateBack({
          
        })
      },1500)
    })
  },

  // 监听银行名称
  nameChange (e) {
    let value = e.detail.value;
    this.setData({
      bankName: value
    })
  },

  // 监听地址
  addressChange (e) {
    let value = e.detail.value;
    this.setData({
      bankAddress: value
    })
  },  

  // 监听银行卡
  cardChange(e) {
    let value = e.detail.value;
    this.setData({
      bankCard: value
    })
  },

  codeChange (e) {
    let value = e.detail.value;
    this.setData({
      bindCode: value
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})