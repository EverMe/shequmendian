import { config } from '../../../utils/config.js';
import { HTTP } from '../../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */ 
  data: {
    monthList: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
    dayList: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
    page: 1,//当前页
    record: [],//数据列表
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // let d = new Date();
    // let day = d.getDate();
    // let month = d.getMonth() + 1;
    // let year = d.getFullYear();
    let date = options.date;
    let day = date.slice(-3,-1);
    let month = date.slice(5,7);
    let year = date.slice(0,4);
    console.log(day, month,year)
    this.setData({
      day: day,
      month: month,
      year: year,
      dateMonth: `${year}-${month}-${day}`
    })
    this.moneyDetail()
    wx.setStorageSync('dayList', this.data.dayList)
    this.dayList()
    // 存储当前月份天数
    this.setData({
      dayNum: this.data.dayList.length,
    })
  },

  // 请求收益明细数据
  moneyDetail: function () {
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    let params = {
      url: config.dayincomedetail,
      data: {
        token: wx.getStorageSync('token'),
        dateTime: this.data.dateMonth,
        page: this.data.page,
        limit: 15,
      }
    }
    http.request(params, (res) => {
      wx.hideLoading()
      let list = res.data.list;
      if(!list[0]) {
        if(this.data.page != 1) {
          wx.showToast({
            title: '没有更多记录了',
            icon: 'none'
          })
        }
        
      }
      for(let i = 0; i < list.length; i++) {
        list[i]['time'] = list[i].datePuton.slice(-8)
        this.data.record.push(list[i])
      }
      this.setData({
        record: this.data.record,
        amount: res.data.total/100
      })
    })
  },

  // 加载更多
  loadMore () {
    this.setData({
      page: this.data.page + 1
    })
    this.moneyDetail()
  },

  // 选择时间
  bindDateChange: function (e) {
    console.log(e.detail.value)
    let date = e.detail.value;
    this.setData({
      year: +date.slice(0, 4),
      dateMonth: `${+date.slice(0, 4)}-${+this.data.month}`
    })
    this.moneyDetail()
    this.dayList()
    this.setData({
      dayNum: this.data.dayList.length,
    })
  },

  // 月份选择
  selectMonth: function (e) {
    // 存储当前选择月份
    this.setData({
      month: this.data.monthList[e.detail.value]
    })
    // 获取当月天数
    this.dayList()
    // 存储天数最后一日并存储年月参数
    this.setData({
      // day: this.data.dayList[this.data.dayList.length - 1],
      dateMonth: `${this.data.year}-${this.data.month}-${this.data.day}`,
      record: []
    })
    // 请求当前时间收益数据
    this.moneyDetail()
    // 数据置顶
    this.setData({
      toTop: 0
    })
    this.setData({
      dayNum: this.data.dayList.length,
    })
  },

  // 选择日
  selectDay: function (e) {
    let index = e.detail.value;
    // let scorllNum = parseInt(index) + 1;
    this.setData({
      page: 1,
      day: this.data.dayList[index],
      dateMonth: `${this.data.year}-${this.data.month}-${this.data.dayList[index]}`,
      record: []
    })
    this.moneyDetail()
    this.setData({
      toTop: 0
    })

  },

  // 判断有几日
  dayList: function () {
    let year = this.data.year;
    let month = this.data.month;
    if (year % 4 == 0 && month == 2) {
      let newList = wx.getStorageSync('dayList')
      newList.splice(29, 4)
      this.setData({
        dayList: newList
      })
    }
    else {
      if (month == 2) {
        let newList = wx.getStorageSync('dayList')
        newList.splice(28, 4)
        this.setData({
          dayList: newList
        })
      }
      else if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
        this.setData({
          dayList: wx.getStorageSync('dayList')
        })
      }

      else {
        let newList = wx.getStorageSync('dayList')
        newList.splice(30, 1)
        this.setData({
          dayList: newList
        })
      }
    }
  },

  // 跳转每单详情
  toExpressDetail (e) {
    let expressOrderId = e.currentTarget.dataset.expressid;
    wx.navigateTo({
      url: '/pages/vallet/incomeDetail/incomeDetail?expressOrderId=' + expressOrderId,
    })
  },

  onPullDownRefresh: function () {
    wx.showNavigationBarLoading() //在标题栏中显示加载
    //模拟加载
    setTimeout(function () {
      // complete
      wx.hideNavigationBarLoading() //完成停止加载
      wx.stopPullDownRefresh() //停止下拉刷新
    }, 1500);

  },
})