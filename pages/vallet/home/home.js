// pages/vallet/home.js
import {
  config
} from '../../../utils/config.js'
import {
  HTTP
} from '../../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    walletInfo: '',//钱包信息
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let params = {
      url: config.getbriefamount,
      data: {
        token: wx.getStorageSync('token')
      }
    }
    http.request(params,(res)=> {
      let data = res.data.walletInfo;
      data['tail'] = data.bankCard.slice(-4);
      
      this.setData({
        walletInfo: data
      })
    })
  },

  // 前往每月明细
  toMonthlyDetail () {
    wx.navigateTo({
      url: '/pages/vallet/income/income'
    })
  },

  // 前往管理银行卡
  toCard (e) {
    let cardInfo = e.currentTarget.dataset.item;
    wx.navigateTo({
      url: '/pages/vallet/bankCard/card?cardInfo=' + JSON.stringify(cardInfo),
    })
  },

  // 前往提现页面
  toExtract () {
    wx.navigateTo({
      url: '/pages/vallet/extract/extract',
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  onPullDownRefresh: function() {
    wx.showNavigationBarLoading() //在标题栏中显示加载
    this.onShow()
    //模拟加载
    setTimeout(function () {
      // complete
      wx.hideNavigationBarLoading() //完成停止加载
      wx.stopPullDownRefresh() //停止下拉刷新
    }, 1500);
  }
})