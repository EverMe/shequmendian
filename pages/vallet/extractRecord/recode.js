// pages/vallet/extractRecord/recode.js
import {
  config
} from '../../../utils/config.js'
import {
  HTTP
} from '../../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    page: 1,
    withdrawhistory: [], //提现记录
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getRecord()
  },

  // 获取提现记录
  getRecord() {
    wx.showLoading({
      title: '加载中'
    })
    let params = {
      url: config.withdrawhistory,
      data: {
        token: wx.getStorageSync('token'),
        page: this.data.page,
        limit: 15
      }
    }
    http.request(params, (res) => {
      wx.hideLoading()
      let data = res.data.list;
      if (!data[0]) {
        if (this.data.page != 1) {
          wx.showToast({
            title: '没有更多记录了',
            icon: 'none'
          })
        }
      }
      let withdrawhistory = this.data.withdrawhistory;
      for (let i = 0; i < data.length; i++) {
        withdrawhistory.push(data[i])
      }
      this.setData({
        withdrawhistory: withdrawhistory
      })
    })
  },

  // 加载更多
  loadMore () {
    this.setData({
      page: this.data.page + 1
    })
    this.getRecord()
  },

  // 跳转提现详情
  toDetail(e) {
    let detail = e.currentTarget.dataset.item;
    wx.navigateTo({
      url: '/pages/vallet/extractDetail/detail?detail=' + JSON.stringify(detail),
    })
  },



  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})