// pages/vallet/extract/extract.js
import {
  config
} from '../../../utils/config.js'
import {
  HTTP
} from '../../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pass: false,//提现按钮是否可操作
    loading: false,
    balance: '',//余额
    moneyInput: '',//输入金额
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let params = {
      url: config.bankcardinfo,
      data: {
        token: wx.getStorageSync('token')
      }
    }
    http.request(params, (res) => {
      let cardInfo = res.data.bankInfo;
      if (!cardInfo) {
        wx.navigateTo({
          url: '/pages/vallet/bankCard/card',
        })
      }
      cardInfo['tail'] = cardInfo && cardInfo.bankCard.slice(-4);
      this.setData({
        cardInfo: cardInfo,
        balance: cardInfo.accInc
      })
    })
  },

  // 获取输入金额
  moneyChange (e) {
    let value = + e.detail.value;
    if (value >= 50) {
      this.setData({
        pass: true,
        moneyInput: value
      })
    }
    else {
      this.setData({
        pass: false
      })
    }
    
  },

  // 全部提现
  withdrawAll () {
    this.setData({
      moneyInput: this.data.balance/100
    })  
    if (this.data.moneyInput >= 50) {
      this.setData({
        pass: true
      })
    }
  },


  // 提现申请
  withdraw () {
    if(this.data.pass) {
      this.setData({
        loading: true
      })
      let params = {
        url: config.withdraw,
        data: {
          token: wx.getStorageSync('token'),
          cash: this.data.moneyInput* 100
        }
      }
      http.request(params,(res)=> {
        wx.navigateTo({
          url: '/pages/success/success',
        })
        this.setData({
          moneyInput: '',
          pass: false,
          loading: false
        })
      })
      
    }
    setTimeout(()=> {
      this.setData({
        loading: false
      })
    },3000)
    
  },

  // 前往记录
  toRecord () {
    wx.navigateTo({
      url: '/pages/vallet/extractRecord/recode',
    })
  },

  // 跳转银行卡
  toBankCard () {
    wx.navigateTo({
      url: '/pages/vallet/bankCard/card',
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})