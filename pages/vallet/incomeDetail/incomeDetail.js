// pages/vallet/incomeDetail/incomeDetail.js
import { config } from '../../../utils/config.js';
import { HTTP } from '../../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    expressOrder: '',//单票收入详情
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let expressOrderId = options.expressOrderId
    let params = {
      url: config.orderincomedetail,
      data: {
        token: wx.getStorageSync('token'),
        expressOrderId: expressOrderId
      }
    }
    http.request(params,(res)=> {
      let data = res.data.expressOrder;
      this.setData({
        expressOrder: data
      })
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})