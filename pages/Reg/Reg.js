// var app = getApp()
import { config } from '../../utils/config.js'
import { HTTP } from '../../utils/http.js';
const http = new HTTP();
Page({
  data: {
    getCodeText: '获取验证码',
    getCodeStatus: true,
    code: '',
    phone:''
  },
  onLoad: function (e) {

  },
  onShow: function () {

  },
  formSubmit: function (e) {
    let value = e.detail.value
    let regphone = /^1[0-9]{10}$/;//手机号校验
    let han = /^[\u4e00-\u9fa5]+$/;//姓名校验
    let passwordRg = /[0-9a-z_-]{6,18}///密码校验
    if (value.nickname == "") {
      wx.showToast({
        title: '请输入姓名',
        icon: 'none'
      })
    } else if (!han.test(value.nickname)) {
      wx.showToast({
        title: '请输入汉字姓名',
        duration: 2000,
        icon: 'none'
      })
    }
    else if (value.phone.length == '') {
      wx.showToast({
        title: '请输入手机号',
        duration: 2000,
        icon: 'none'
      })
    } else if (!regphone.test(value.phone)) {
      wx.showToast({
        title: '手机号有误！',
        duration: 2000,
        icon: 'none'
      })
    } else if (value.phone.length < 11) {
      wx.showToast({
        title: '手机号长度有误！',
        duration: 2000,
        icon: 'none'
      })
    }
    else if(!passwordRg.test(value.password)) {
      wx.showToast({
        title: '密码格式有误！',
        duration: 2000,
        icon: 'none'
      })
    }
    else if (value.password != value.passwordAgain) {
      wx.showToast({
        title: '两次密码输入不一致！',
        duration: 2000,
        icon: 'none'
      })
    }
     else {
      console.log('可以注册了')
      wx.showLoading({
        title: '请稍后',
        mask: true
      })
      // 获取jsCode
      wx.login({
        success: res => {
          if (res.code) {
            // console.log(res.code)
            // return;
            let params = {
              url: config.registerv2,
              data: {
                jsCode: res.code,
                nickname: value.nickname,
                phone: value.phone,
                password: value.password
              }
            };
            http.request(params, function (data) {
              console.log(data)
              wx.hideLoading()
              if(data.code === '10000') {
                wx.showToast({
                  title: '注册成功',
                  success:(res)=> {
                    setTimeout(function() {
                      wx.navigateBack({

                      })
                    },1500)
                    
                  }
                })
              }
            });
            setTimeout(function () {
              wx.hideLoading()
            }, 1000)
          } else {
            wx.showModal({
              content: '获取用户登录态失败！' + res.errMsg
            })
          }
        }
      });
      
    }
  },

  // 存储phone
  // phoneInput: function(e) {
  //   let phone = e.detail.value;
  //   this.setData({
  //     phone: phone
  //   })
  // },

  // getCode: function () {
  //   if (that.data.getCodeStatus) {
  //     var regphone = /^1[0-9]{10}$/;//手机号
  //     if (!regphone.test(this.data.phone)) {
  //       wx.showToast({
  //         title: '手机号有误！',
  //         duration: 2000,
  //         icon: 'none'
  //       })
  //       return;
  //     } else if (this.data.phone.length == '') {
  //       wx.showToast({
  //         title: '请输入的手机号',
  //         duration: 2000,
  //         icon: 'none'
  //       })
  //       return;
  //     } else if (this.data.phone.length < 11) {
  //       wx.showToast({
  //         title: '手机号长度有误！',
  //         duration: 2000,
  //         icon: 'none'
  //       })
  //       return;
  //     }

  //     var params = {
  //       url: config.getsms,
  //       data: {
  //         token: wx.getStorageSync('token'),
  //         phone: that.data.phone
  //       }
  //     };

  //     http.request(params, function (data) {
  //       console.log(data)
  //       wx.showToast({
  //         title: '已发送',
  //         duration: 2000
  //       })
  //       time(timeNum);
  //     })

  //     var timeNum = 120;
  //     function time(timeNum) {
  //       that.setData({
  //         getCodeText: timeNum + '秒后重试',
  //         getCodeStatus: false
  //       })
  //       if (timeNum == 0) {
  //         that.setData({
  //           getCodeText: '获取验证码',
  //           getCodeStatus: true
  //         })
  //       }
  //       else {
  //         setTimeout(function () {
  //           timeNum = timeNum - 1;
  //           time(timeNum);
  //         }, 1000)

  //       }
  //     }


  //   }
  //   else {
  //     return;
  //   }

  // }
})