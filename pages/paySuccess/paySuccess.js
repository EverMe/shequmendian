// pages/paySuccess/paySuccess.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    num: 3
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    setTimeout(()=> {
      this.setData({
        num: 2 
      })
    },1000)
    setTimeout(() => {
      this.setData({
        num: 1
      })
    }, 2000)
    setTimeout(()=> {
      if (!this.data.isBack) {
        this.backPage()
      }
    },3000)
  },

  // 返回
  backPage () {
    wx.navigateBack({
      
    })
    this.setData({
      isBack: true
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})