// pages/home/home.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (wx.getStorageSync('info')) {
      
    }
    else {
      wx.redirectTo({
        url: '/pages/login/login'
      })
    }
  },

  // 跳转快递查询
  toExpressQuery: function() {
    wx.navigateTo({
      url: '/pages/ShowEvaluation/evaluate',
    })
  },

  // 跳转柜机管理
  toMouthSearch: function() {
    wx.navigateTo({
      url: '/pages/mouthQuery/query',
    })
  },

  // 跳转价格设置
  toPriceSetting: function() {
    wx.navigateTo({
      url: '/pages/priceSetting/home/home',
    })
  },

  // 跳转逾期设置
  toOverdueSetting: function () {
    wx.navigateTo({
      url: '/pages/overdueSetting/home/home',
    })
  },

  // 跳转预留设置
  toReserveSetting: function () {
    wx.navigateTo({
      url: '/pages/reserveSetting/home/home',
    })
  },

  // 跳转拒收管理
  toRefuse () {
    wx.navigateTo({
      url: '/pages/rejectManage/home/home',
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading() //在标题栏中显示加载
    //模拟加载
    setTimeout(function () {
      // complete
      wx.hideNavigationBarLoading() //完成停止加载
      wx.stopPullDownRefresh() //停止下拉刷新
    }, 1500);
    
  },


  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})