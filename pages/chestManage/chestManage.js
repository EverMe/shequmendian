// pages/chestManage/chestManage.js
import {
  config
} from '../../utils/config.js'
import {
  HTTP
} from '../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    modChest:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
      mask:true
    })
    http.request({ url: config.getcabinetinfo, data: { token: wx.getStorageSync('token') } },(data)=>{
      for(let i = 0; i < data.data.list.length; i ++) {
        let shijianchuo = +data.data.list[i].createDate * 1000;
        let creatDate = new Date(shijianchuo);
        let creatYear = creatDate.getFullYear();
        let creatMonth = creatDate.getMonth() + 1;
        let creatDay = creatDate.getDate();
        data.data.list[i].createDate = `${creatYear}-${creatMonth}-${creatDay}`
      }
      
      this.setData({
        chestList: data.data.list
      })
      wx.hideLoading()
    })
    
  },

  // 获取柜机名称
  getcabName: function(e) {
    let cabName = e.detail.value
    this.setData({
      cabName:cabName
    })
  },

  // 修改快递柜名称
  cabnameMod: function(e) {
    let cabid = e.currentTarget.dataset.cabid;
    let params = {
      url: config.updcabname,
      data:{
        token:wx.getStorageSync('token'),
        cabId:cabid,
        cabName:this.data.cabName,
      }
    }
    http.request(params,(data)=>{
      wx.showToast({
        title: '修改成功',
      })
    })
  },

  // 暂不修改
  closeModel: function() {
    this.setData({
      modChest:false
    })
  },

  // 打开修改模态窗
  openModel: function(e) {
    let item = e.currentTarget.dataset.item;
    this.setData({
      modChest:true,
      selectedChest:item
    })
  },

  // 获取快递柜名称
  cabName: function(e) {
    this.setData({
      cabName:e.detail.value
    })
  },

  // 获取地址
  address: function (e) {
    this.setData({
      address: e.detail.value
    })
  },

  // 确认修改
  sureMod: function(e) {
    let cabName = e.detail.value.cabName;
    let address = e.detail.value.address;
    if (!cabName) {
      wx.showToast({
        title: '柜机名不能为空',
        icon:'none',
        duration:2000,
      })
    }
    else if (!cabName) {
      wx.showToast({
        title: '地址不能为空',
        icon: 'none',
        duration: 2000,
      })
    }
    else {
      let params = {
        url: config.updcabname,
        data:{
          token: wx.getStorageSync('token'),
          cabId: this.data.selectedChest.cabinet_id,
          cabName: cabName,
          address:address,
        }
        
      }
      http.request(params,(data)=>{
       this.setData({
         modChest: false
       })
       this.onLoad();
       wx.showToast({
         title: '修改成功',
       })
      })
    }
  },
 
})