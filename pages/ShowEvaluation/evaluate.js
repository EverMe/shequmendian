// pages/ShowEvaluation/evaluate.js
import {
  config
} from '../../utils/config.js'
import {
  HTTP
} from '../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabList: ['已逾期', '待自提', '已完成'],
    current: 0,
    switchTab: [], //详细订单的展开状态(滞留件)
    switchTab2: [], //详细订单的展开状态（待取件）
    switchTab3: [], //详细订单的展开状态（已完成）
    showpage: true,
    canSelectTime: ['最近1周', '最近2周', '最近3周', '最近1月'],
    timeWindowstatus: false, //是否显示选择时间窗口
    selectedTime: '全部', //默认 最近1周
    keyvalue: [2, 3],
    sureSetPhone: false, //是否可以操作确认修改电话号码
    getPhone: 15797769097,
    firstReset: true, //初始状态出现清空按钮
    PhoneModel: false, //是否显示修改手机model
    phoneFocus: true, //输入框光标
    chestRecord: false, //入柜记录显示状态
    numList: [], //各类订单数量
    openSuccess: false, //开柜成功模态窗默认状态
    SIGNEDcanScroll: true, //地址所在scroll-view默认可以上下滑动(已完成)
    OVERDUEcanScroll: true, //地址所在scroll-view默认可以上下滑动(已逾期)
    WAITTAKEcanScroll: true, //地址所在scroll-view默认可以上下滑动（待自提）
    OVERDUE2: [], //用于存放已逾期件的初始化数组
    WAITTAKE2: [], //用于存放待自提件的初始化数组
    SIGNED2: [], //用于存放已完成件的初始化数组
    OVERDUEpage: [], //已逾期件的初始化页数
    WAITTAKEpage: [], //待自提件的初始化页数
    SIGNEDpage: [], //已完成件的初始化页数
    reSendStatus: false, //冲发短信弹窗的状态
    comModel: false, //是否展开快递品牌（逾期件）
    comModel2: false, //是否展开快递品牌（待自提）
    dateArray: ['三天', '七天', '十五天'],
    state:'OVERDUE',//默认页面已逾期
    adapHeight: "",//适配高度
  },

  // 监听轮播页
  swiperChange: function(e) {
    this.setData({
      current: e.detail.current
    })
    this.getCompanyState();
  },

  // 点击tab
  selectTab: function(e) {
    var index = e.currentTarget.dataset.index;
    this.setData({
      current: index
    })
  },

  getCompanyState: function() {
    let state;
    let index = this.data.current;

    if (index == 0) {
      state = 'OVERDUE';
    }
    else if (index == 1) {
      state = 'WAITTAKE';
    }
    else if (index == 2) {
      state = 'HAVETAKEN';
    }
    else {
      state = 'OVERDUE';
    }

    let params = {
      url: config.getexpresscom,
      data: {
        token: wx.getStorageSync('token'),
        state: state,
        startTime: this.data.serverStartDate,
        endTime: this.data.serverEndDate
      }
    }
    http.request(params, (data) => {
      if(data.data === 'true') {
        wx.hideLoading()
        wx.showModal({
          title:'提醒',
          content:'您暂时没有绑定柜机，请联系客服绑定柜机后重试！',
          showCancel:false,
          confirmText:'我知道了'
        })
        return;
      }
      data.data.list.unshift({
        comName: '全部',
        id: ''
      })
      this.setData({
        comList: data.data.list
      })
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    wx.getSystemInfo({
      success(res) {
        let adapHeight = res.windowHeight;
        if (adapHeight > 700 && adapHeight < 1000) {
          that.setData({
            adapHeight:  '1060rpx'
          })
        }
        else if (adapHeight > 1000) {
          that.setData({
            adapHeight: '540rpx'
          })
        }
        
      }
    })
    wx.showLoading({
      mask: true,
    })
    // 当天时间
    let myDate = new Date();
    let year = myDate.getFullYear();
    let month = myDate.getMonth() + 1;
    let day = myDate.getDate();

    // 计算6天前时间
    let timestamp = Date.parse(myDate) - 6*24*3600*1000;//七天前时间戳
    let startDate = new Date(timestamp);//6天前年月日
    let startYear = startDate.getFullYear();
    let startMonth = startDate.getMonth() + 1;
    let startDay = startDate.getDate();


    this.setData({
      startDate: `${startYear}年${startMonth}月${startDay}日`,
      endDate: `${year}年${month}月${day}日`,

      // 存储日期年月日
      startYear: startYear,
      startMonth: startMonth,
      startDay: startDay,
      endYear: year,
      endMonth: month,
      endDay: day,

      // 存储传到服务器的时间
      serverStartDate: `${startYear}-${startMonth}-${startDay}`,
      serverEndDate: `${year}-${month}-${day}`,
    })
    this.loginLater()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

// 登录之后需要请求的数据
  loginLater:function() {
    wx.showLoading({
      title: '加载中',
      mask:true
    })
    this.getCompanyState()//请求快递公司列表

    // 请求柜子地址列表
    let params2 = {
      url: config.getcabinet,
      data: {
        token: wx.getStorageSync('token'),
        comId: this.data.comId ? this.data.comId : '',
        startTime: this.data.serverStartDate,
        endTime: this.data.serverEndDate
      }
    }
    http.request(params2, (data) => {
      
      // 各类型订单的数量
      this.data.numList[0] = data.data.row.overdue;
      this.data.numList[1] = data.data.row.waitTakeCount;
      this.data.numList[2] = data.data.row.haveTakeCount;
      this.setData({
        numList: this.data.numList
      })

      // 柜子数据
      this.setData({
        allChest: data.data.list
      })
      for (let i = 0; i < this.data.allChest.length; i++) {
        this.data.OVERDUEpage[i] = 1;
        this.data.WAITTAKEpage[i] = 1;
        this.data.SIGNEDpage[i] = 1;
        // 已逾期数据处理
        if (this.data.allChest[i].overdue != '0') {
          this.setData({
            hideOVERDUE: true, //存在非零就隐藏无订单提示
          })
        }
        // 待自提数据处理
        if (this.data.allChest[i].waitTakeCount != '0') {
          this.setData({
            hideWAITTAKE: true, //存在非零就隐藏无订单提示
          })
        }
        // 已完成数据处理
        if (this.data.allChest[i].haveTakeCount != '0') {
          this.setData({
            hideSIGNED: true, //存在非零就隐藏无订单提示
          })
        }
        this.setData({
          OVERDUEpage: this.data.OVERDUEpage,
          WAITTAKEpage: this.data.WAITTAKEpage,
          SIGNEDpage: this.data.SIGNEDpage,
        })
        
      }
      wx.hideLoading()
      wx.hideNavigationBarLoading() //完成停止加载
      wx.stopPullDownRefresh() //停止下拉刷新
    })
  },

  // 是否展开订单详情(已逾期件)
  switchTab: function(e) {
    var bindindex = e.currentTarget.dataset.index;
    var cabid = e.currentTarget.dataset.cabid;
    var comId = this.data.comId
    this.setData({
      bindindex: bindindex,
      cabid: cabid,
      OVERDUEcanScroll: true
    })
    let params = {
      url: config.searchpackage,
      data: {
        token: wx.getStorageSync('token'),
        cabId: cabid,
        state: 'OVERDUE',
        page: '1',
        size: '10',
        comId: comId ? comId : '',
        startTime: this.data.serverStartDate,
        endTime: this.data.serverEndDate
      }

    }
    if (!this.data.OVERDUE2[bindindex]) {
      wx.showLoading({
        mask: true
      })
      http.request(params, (data) => {
        wx.hideLoading()
        let OVERDUE2 = `OVERDUE2[${bindindex}]`
        this.setData({
          [OVERDUE2]: data.data.list,
        })
        this.methods_OVERDUE()
      })
    } else {
      this.methods_OVERDUE()
    }

  },

  // 逾期件订单展开交互方法
  methods_OVERDUE: function() {
    let bindindex = this.data.bindindex
    // 排它处理，一次只能打开一个地址
    for (let i = 0; i < this.data.allChest.length; i++) {
      if (i == bindindex) {
        this.data.switchTab[i] = !this.data.switchTab[i]
      } else {
        this.data.switchTab[i] = false
      }
    }
    this.setData({
      switchTab: this.data.switchTab,
    })
    this.setData({
      OVERDUEscrollId: 'scrollId' + bindindex
    })

    if (this.data.switchTab[bindindex] == true) {
      this.setData({
        OVERDUEcanScroll: false
      })
    } else {
      this.setData({
        OVERDUEcanScroll: true
      })
    }
  },

  // 是否展开订单详情(待取件)
  switchTab2: function(e) {
    var bindindex2 = e.currentTarget.dataset.index;
    var cabid = e.currentTarget.dataset.cabid;
    var comId = this.data.comId
    this.setData({
      bindindex2: bindindex2,
      cabid2: cabid,
      WAITTAKEcanScroll: true
    })
    let params = {
      url: config.searchpackage,
      data: {
        token: wx.getStorageSync('token'),
        cabId: cabid,
        state: 'WAITTAKE',
        page: '1',
        size: '10',
        comId: comId ? comId : '',
        startTime: this.data.serverStartDate,
        endTime: this.data.serverEndDate
      }
    }
    if (!this.data.WAITTAKE2[bindindex2]) {
      wx.showLoading({
        mask: true
      })
      http.request(params, (data) => {
        wx.hideLoading()
        let WAITTAKE2 = `WAITTAKE2[${bindindex2}]`
        this.setData({
          [WAITTAKE2]: data.data.list,
        })
        this.methods_WAITTAKE()

      })
    } else {
      this.methods_WAITTAKE()
    }
  },

  // 待自提订单展开交互方法
  methods_WAITTAKE: function() {
    let bindindex2 = this.data.bindindex2
    // 排它处理，一次只能打开一个地址
    for (let i = 0; i < this.data.allChest.length; i++) {
      if (i == bindindex2) {
        this.data.switchTab2[i] = !this.data.switchTab2[i]
      } else {
        this.data.switchTab2[i] = false
      }
    }
    this.setData({
      switchTab2: this.data.switchTab2,
    })
    this.setData({
      WAITTAKEscrollId: 'scrollId' + bindindex2
    })

    if (this.data.switchTab2[bindindex2] == true) {
      this.setData({
        WAITTAKEcanScroll: false
      })
    } else {
      this.setData({
        WAITTAKEcanScroll: true
      })
    }

  },

  // 是否展开订单详情(已完成)
  switchTab3: function(e) {

    var bindindex3 = e.currentTarget.dataset.index;
    var cabid = e.currentTarget.dataset.cabid;
    var comId = this.data.comId;
    let dateFilter = this.data.dateFilter
    this.setData({
      bindindex3: bindindex3,
      cabid3: cabid,
      SIGNEDcanScroll: true
    })
    let params = {
      url: config.searchpackage,
      data: {
        token: wx.getStorageSync('token'),
        cabId: cabid,
        state: 'HAVETAKEN',
        page: '1',
        size: '10',
        comId: comId ? comId : '',
        startTime: this.data.serverStartDate,
        endTime: this.data.serverEndDate
      }

    }
    if (!this.data.SIGNED2[bindindex3]) {
      wx.showLoading({
        mask: true
      })
      http.request(params, (data) => {
        wx.hideLoading()
        let SIGNED2 = `SIGNED2[${bindindex3}]`
        this.setData({
          [SIGNED2]: data.data.list,
        })
        this.methods_SIGNED()

      })
    } else {
      this.methods_SIGNED()
    }
  },

  // 已完成订单展开交互方法
  methods_SIGNED: function() {
    let bindindex3 = this.data.bindindex3
    // 排它处理，一次只能打开一个地址
    for (let i = 0; i < this.data.allChest.length; i++) {
      if (i == bindindex3) {
        this.data.switchTab3[i] = !this.data.switchTab3[i];
      } else {
        this.data.switchTab3[i] = false
      }
    }
    this.setData({
      switchTab3: this.data.switchTab3,
    })
    this.setData({
      SIGNEDscrollId: 'scrollId' + bindindex3
    })

    if (this.data.switchTab3[bindindex3] == true) {
      this.setData({
        SIGNEDcanScroll: false
      })
    } else {
      this.setData({
        SIGNEDcanScroll: true
      })
    }

  },

  // 逾期件加载更多

  OVERDUELoadMore: function() {
    wx.showLoading({
      title: '加载中',
      mask: true
    })

    let bindindex = this.data.bindindex;
    let cabid = this.data.cabid;
    let comId = this.data.comId
    this.data.OVERDUEpage[bindindex]++;
    let params = {
      url: config.searchpackage,
      data: {
        token: wx.getStorageSync('token'),
        cabId: cabid,
        state: 'OVERDUE',
        page: this.data.OVERDUEpage[bindindex],
        size: '10',
        comId: comId ? comId : '',
        startTime: this.data.serverStartDate,
        endTime: this.data.serverEndDate
      }

    }
    http.request(params, (data) => {
      wx.hideLoading()
      if (!data.data.list[0]) {
        wx.showToast({
          title: '没有更多内容了',
          icon: 'none',
          duration: 1500,
        })
        return;
      }
      for (let i = 0; i < data.data.list.length; i++) {
        this.data.OVERDUE2[bindindex].push(data.data.list[i])
      }

      this.setData({
        OVERDUE2: this.data.OVERDUE2
      })

    })

  },

  // 待自提加载更多

  WAITTAKELoadMore: function() {
    wx.showLoading({
      title: '加载中',
      mask: true
    })

    let bindindex2 = this.data.bindindex2;
    let cabid2 = this.data.cabid2;
    let comId = this.data.comId;
    this.data.WAITTAKEpage[bindindex2]++;
    let params = {
      url: config.searchpackage,
      data: {
        token: wx.getStorageSync('token'),
        cabId: cabid2,
        state: 'WAITTAKE',
        page: this.data.WAITTAKEpage[bindindex2],
        size: '10',
        comId: comId ? comId : '',
        startTime: this.data.serverStartDate,
        endTime: this.data.serverEndDate
      }

    }
    http.request(params, (data) => {
      wx.hideLoading()
      if (!data.data.list[0]) {
        wx.showToast({
          title: '没有更多内容了',
          icon: 'none',
          duration: 1500,
        })
        return;
      }
      for (let i = 0; i < data.data.list.length; i++) {
        this.data.WAITTAKE2[bindindex2].push(data.data.list[i])
      }
      this.setData({
        WAITTAKE2: this.data.WAITTAKE2
      })

    })
  },

  // 已完成加载更多

  SIGNEDLoadMore: function() {
    wx.showLoading({
      title: '加载中',
      mask: true
    })

    let bindindex3 = this.data.bindindex3;
    let cabid3 = this.data.cabid3;
    let comId = this.data.comId;
    let dateFilter = this.data.dateFilter
    this.data.SIGNEDpage[bindindex3]++;
    let params = {
      url: config.searchpackage,
      data: {
        token: wx.getStorageSync('token'),
        cabId: cabid3,
        state: 'HAVETAKEN',
        page: this.data.SIGNEDpage[bindindex3],
        size: '10',
        comId: comId ? comId : '',
        startTime: this.data.serverStartDate,
        endTime: this.data.serverEndDate
      }

    }
    http.request(params, (data) => {
      wx.hideLoading()
      if (!data.data.list[0]) {
        wx.showToast({
          title: '没有更多内容了',
          icon: 'none',
          duration: 1500,
        })
        return;
      }
      for (let i = 0; i < data.data.list.length; i++) {
        this.data.SIGNED2[bindindex3].push(data.data.list[i])
      }

      this.setData({
        SIGNED2: this.data.SIGNED2
      })

    })
  },


  // 去详情页
  toDetail: function(e) {
    let detail = e.currentTarget.dataset.item
    detail['epid'] = detail.expressNum;
    detail['datePuton'] = detail.putOnDate;
    detail['datePulloff'] = detail.pullOffDate;

    wx.navigateTo({
      url: '/pages/waybillDetail/detail?detail=' + JSON.stringify(detail),
    })
  },

  // 修改手机的监听事件
  inputBind: function(e) {
    let phone = e.detail.value;
    if (phone.length == 11) {
      this.setData({
        phoneNumber: phone,
        sureSetPhone: true,
        firstReset: false
      })
    } else {
      this.setData({
        sureSetPhone: false,
        firstReset: false
      })
    }
  },

  // 删除输入的手机号
  resetPhone: function() {
    this.setData({
      sureSetPhone: false,
      needSetphone: '',
      firstReset: false,
      phoneFocus: true,
    })
  },

  // 暂不修改
  closePhoneModel: function() {
    this.setData({
      PhoneModel: false
    })
  },

  // 弹出修改手机model
  updatePhone: function(e) {
    var phone = e.currentTarget.dataset.phone;
    var expressid = e.currentTarget.dataset.expressid;
    var bigindex = e.currentTarget.dataset.bigindex;
    var index = e.currentTarget.dataset.smindex;

    this.setData({
      PhoneModel: true,
      needSetphone: phone,
      getexpressId: expressid,
      bigindex: bigindex,
      smindex: index,
    })

  },

  // 重发短信取件码
  rsendNote: function(e) {
    var expressid = e.currentTarget.dataset.expressid
    this.setData({
      noteExpressid: expressid,
      reSendStatus: true,
    })
  },

  // 确定发送短信
  sureSend: function() {
    this.setData({
      reSendStatus: false
    })
    app.post('resendmsg', {
      token: wx.getStorageSync('token'),
      expressId: this.data.noteExpressid
    }, (data) => {

      wx.showToast({
        title: '发送成功',
      })
    })
  },

  // 取消重发短信
  cancleSend: function() {
    this.setData({
      reSendStatus: false
    })
  },

  // 进入运单详情页
  openRecodeModel: function(e) {
    let detail = e.currentTarget.dataset.detail;
    let current = e.currentTarget.dataset.current;
    let bgindex = e.currentTarget.dataset.bgindex;
    let smindex = e.currentTarget.dataset.smindex;
    this.setData({
      back_data: {
        back_current: current,
        back_bgindex: bgindex,
        back_smindex: smindex,
      }

    })
    wx.navigateTo({
      url: '/pages/waybillDetail/detail?detail=' + JSON.stringify(detail) + '&backData=' + JSON.stringify(this.data.back_data),
    })
  },

  // 跳转查询页面
  toSearch: function() {
    wx.navigateTo({
      url: '/pages/condQuery/query',
    })
  },

  // 拨打电话
  callPhone: function(e) {

    var phone = e.currentTarget.dataset.phone;

    wx.makePhoneCall({
      phoneNumber: phone
    })
  },

  // 确认修改手机号

  sureSet: function() {
    var that = this;
    if (this.data.sureSetPhone) {

      app.post('updphone', {
        token: wx.getStorageSync('token'),
        expressId: that.data.getexpressId,
        phone: that.data.phoneNumber
      }, (data) => {
        this.setData({
          PhoneModel: false
        })
        if (this.data.current == 0) {
          this.data.OVERDUE2[this.data.bigindex][this.data.smindex].phone = that.data.phoneNumber;
          this.setData({
            OVERDUE2: this.data.OVERDUE2
          })
        } else {
          this.data.WAITTAKE2[this.data.bigindex][this.data.smindex].phone = that.data.phoneNumber;
          this.setData({
            WAITTAKE2: this.data.WAITTAKE2
          })
        }
        wx.showToast({
          title: '修改成功',
        })

      })
    }

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    wx.showNavigationBarLoading() //在标题栏中显示加载
    this.setData({
      switchTab: [], //详细订单的展开状态(滞留件)
      switchTab2: [], //详细订单的展开状态（待取件）
      switchTab3: [], //详细订单的展开状态（已完成）
      OVERDUE2: [], //用于存放已逾期件的初始化数组
      WAITTAKE2: [], //用于存放待自提件的初始化数组
      SIGNED2: [], //用于存放已完成件的初始化数组
    })
    this.onLoad();
  },

  // 撤回
  callback: function(e) {
    var batchId = e.currentTarget.dataset.batchid;
    var bigindex = e.currentTarget.dataset.bigindex;
    var smindex = e.currentTarget.dataset.smindex;
    var boxCode = e.currentTarget.dataset.boxcode;
    this.setData({
      openSuccessCode: boxCode,
    })
    wx.showModal({
      title: '开柜撤件提醒',
      content: '您即将打开格口并撤回此快件，请您务必确定位于柜机现场，请确定是否开柜撤件？',
      cancelText: '取消',
      cancelColor: '#A8A8A8',
      confirmText: '确定开柜',
      confirmColor: '#EF5D0F',
      success: (res) => {
        if (res.confirm) {
          wx.showLoading({
            title: '请稍后',
            mask: true
          })
          app.post('cancelbatch', {
            token: wx.getStorageSync('token'),
            batchId: batchId
          }, (data) => {
            wx.hideLoading()
            this.onShow();
            // 请求各类订单数量
            app.post('chestNum', {
              token: wx.getStorageSync('token')
            }, (data) => {
              let numList = []
              numList.push(data.overdue)
              numList.push(data.waitTakeCount)
              numList.push(data.haveTakeCount)
              this.setData({
                numList: numList
              })

            })
            this.setData({
              openSuccess: true,

            })
            if (this.data.current == 0) {
              this.data.OVERDUE2[bigindex].splice(smindex, 1)
              this.setData({
                OVERDUE2: this.data.OVERDUE2
              })
            } else {

              this.data.WAITTAKE2[bigindex].splice(smindex, 1)
              this.setData({
                WAITTAKE2: this.data.WAITTAKE2
              })
            }

          })
        }
      }
    })

  },

  // 关闭模态窗
  closeMengban: function() {
    this.setData({
      openSuccess: false,
    })
  },

  // 是否开启快递模态窗
  comClick: function() {
    this.setData({
      comModel: !this.data.comModel
    })
  },

  // 是否开启时间模态窗
  dateClick: function() {
    this.setData({
      dateModel: !this.data.dateModel,
      comModel: false,
    })
  },

  // 选择快递公司（
  selectedCom: function(e) {
    let comName = e.currentTarget.dataset.comname.comName;
    let comId = e.currentTarget.dataset.comname.id;
    this.setData({
      selectedCom: comName,
      comId: comId,
      comModel: false,
      OVERDUE2: [],
      WAITTAKE2: [],
      SIGNED2: [],
      switchTab: [], //详细订单的展开状态(滞留件)
      switchTab2: [], //详细订单的展开状态（待取件）
      switchTab3: [], //详细订单的展开状态（已完成）
      hideOVERDUE: false,
      hideWAITTAKE: false,
      hideSIGNED: false,
    })
    this.loginLater()
  },


  // 关闭快递公司/时间模态窗
  closeModels: function(e) {
    this.setData({
      comModel: false,
      dateModel: false,
    })
  },

  // 选择开始时间
  startDateChange: function(e) {
    let value = e.detail.value;
    let year = +value.slice(0, 4);
    let month = +value.slice(5, 7);
    let day = +value.slice(8);
    let startDate = `${year}年${month}月${day}日`
    this.setData({
      startDate: startDate,
      // 记录开始年月日
      startYear: year,
      startMonth: month,
      startDay: day,
    })
  },

  // 选择结束时间
  endDateChange: function(e) {
    let value = e.detail.value;
    let year = +value.slice(0, 4);
    let month = +value.slice(5, 7);
    let day = +value.slice(8);
    let endDate = `${year}年${month}月${day}日`
    this.setData({
      endDate: endDate,

      // 记录开始年月日
      endYear: year,
      endMonth: month,
      endDay: day,
    })
  },

  // 筛选时间
  screenDate: function() {
    let startYear = this.data.startYear;
    let startMonth = this.data.startMonth;
    let startDay = this.data.startDay;
    let endYear = this.data.endYear;
    let endMonth = this.data.endMonth;
    let endDay = this.data.endDay;
    if (startYear > endYear || startYear === endYear && startMonth > endMonth || startYear === endYear && startMonth === endMonth && startDay > endDay) {
      wx.showToast({
        title: '开始时间不能大于结束时间,请重新选择',
        icon: 'none',
        duration: 3000
      })
    } else {
      this.setData({
        dateModel: false,
        serverStartDate: `${startYear}-${startMonth}-${startDay}`,
        serverEndDate: `${endYear}-${endMonth}-${endDay}`,
        hideOVERDUE: false,
        hideWAITTAKE: false,
        hideSIGNED: false,
        OVERDUE2: [],
        WAITTAKE2: [],
        SIGNED2: [],
        switchTab: [], //详细订单的展开状态(滞留件)
        switchTab2: [], //详细订单的展开状态（待取件）
        switchTab3: [], //详细订单的展开状态（已完成）
      })
      this.loginLater()
    }
  },
  onShareAppMessage:function() {
    
  }
})