// 柜子列表请求并处理数据备份

let params = {
  token: wx.getStorageSync("token"),
  filter: 0,
}
app.post('ChestList', params, (data) => {
  console.log(data)
  var OVERDUE = [];
  var SIGNED = [];
  var WAITTAKE = [];

  var OVERDUE2 = [];
  var SIGNED2 = [];
  var WAITTAKE2 = [];

  var OVERDUE_type = [];
  var WAITTAKE_type = [];
  var SIGNED_type = [];
  for (let i = 0; i < data.length; i++) {
    if (data[i].state == "OVERDUE") {
      OVERDUE.push(data[i])
    }

    else if (data[i].state == "WAITTAKE") {
      WAITTAKE.push(data[i])
    }

    else if (data[i].state == "SIGNED") {
      SIGNED.push(data[i])
    }
  }
  // 循环出滞留件地址
  for (let i = 0; i < OVERDUE.length; i++) {
    OVERDUE_type.push(OVERDUE[i].cabName)
  }
  // 循环出待取件地址
  for (let i = 0; i < WAITTAKE.length; i++) {
    WAITTAKE_type.push(WAITTAKE[i].cabName)
  }
  // 循环出已完成地址
  for (let i = 0; i < SIGNED.length; i++) {
    SIGNED_type.push(SIGNED[i].cabName)
  }

  OVERDUE_type = Array.from(new Set(OVERDUE_type))//滞留件去重
  // 为真实的二层对象创建空数组
  for (let i = 0; i < OVERDUE_type.length; i++) {
    OVERDUE2[i] = [];
    this.data.switchTab[i] = false;//详细订单的展开状态初始化(滞留件)

  }
  WAITTAKE_type = Array.from(new Set(WAITTAKE_type))//待取件去重
  // 为真实的二层对象创建空数组
  for (let i = 0; i < WAITTAKE_type.length; i++) {
    WAITTAKE2[i] = []
    this.data.switchTab2[i] = false;//详细订单的展开状态初始化(待自提)
  }
  SIGNED_type = Array.from(new Set(SIGNED_type))//已完成去重
  // 为真实的二层对象创建空数组
  for (let i = 0; i < SIGNED_type.length; i++) {
    SIGNED2[i] = []
    this.data.switchTab3[i] = false;//详细订单的展开状态初始化(滞留件)
  }
  // 循环出滞留件（OVERDUE）
  for (let i = 0; i < OVERDUE_type.length; i++) {
    for (let j = 0; j < OVERDUE.length; j++) {
      if (OVERDUE[j].cabName == OVERDUE_type[i]) {
        OVERDUE2[i].push(OVERDUE[j])
      }
    }
  }

  // 将逾期件倒序

  var sortBy = function (filed, rev, primer) {
    rev = (rev) ? -1 : 1;
    return function (a, b) {
      a = a[filed];
      b = b[filed];
      if (typeof (primer) != 'undefined') {
        a = primer(a);
        b = primer(b);
      }
      if (a < b) { return rev * -1; }
      if (a > b) { return rev * 1; }
      return 1;
    }
  };
  for (let i = 0; i < OVERDUE2.length; i++) {
    OVERDUE2[i].sort(sortBy('overdueDay', false, parseInt));
    OVERDUE2[i] = OVERDUE2[i].reverse()
  }

  console.log(OVERDUE2)


  // 循环出待取件（WAITTAKE）
  for (let i = 0; i < WAITTAKE_type.length; i++) {
    for (let j = 0; j < WAITTAKE.length; j++) {
      if (WAITTAKE[j].cabName == WAITTAKE_type[i]) {
        WAITTAKE2[i].push(WAITTAKE[j])
      }
    }
  }

  // 循环出已完成（SIGNED）
  for (let i = 0; i < SIGNED_type.length; i++) {
    for (let j = 0; j < SIGNED.length; j++) {
      if (SIGNED[j].cabName == SIGNED_type[i]) {
        SIGNED2[i].push(SIGNED[j])
      }
    }
  }


  this.setData({
    OVERDUE2: OVERDUE2,
    WAITTAKE2: WAITTAKE2,
    SIGNED2: SIGNED2,

    OVERDUE_type: OVERDUE_type,
    WAITTAKE_type: WAITTAKE_type,
    SIGNED_type: SIGNED_type,

    switchTab: this.data.switchTab,//详细订单的展开状态初始化(滞留件)
    switchTab2: this.data.switchTab2,//详细订单的展开状态初始化(待自提)
    switchTab3: this.data.switchTab3,//详细订单的展开状态初始化(已完成)

  })
})