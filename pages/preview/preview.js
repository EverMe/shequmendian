// pages/preview/preview.js
import {
  config
} from '../../utils/config.js'
import {
  HTTP
} from '../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cabPreview: '',//柜机预览数据
    showPreview: false,//是否打开预览图
    cabInfo: '',//柜机信息
    codeValue: '',//格口编号
    colValue: "",//列
    rowValue: "",//行
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let cabInfo = JSON.parse(options.cabInfo);
    this.setData({
      cabInfo: cabInfo
    })
  },

  onShow: function() {
    let params = {
      url: config.viewboxlist,
      data: {
        token: wx.getStorageSync('token'),
        cabId: this.data.cabInfo.id
      }
    }
    http.request(params, (res) => {
      let amount = res.data.amount;
      let list = res.data.list;
      let cabPreview = [];
      for (let i = 0; i < Math.ceil(list.length / 2); i++) {
        cabPreview[i] = [];
        for (let j = 0; j < list.length; j++) {
          if (Math.floor(j / 2) === i) {
            cabPreview[i].push(list[j])
          }
        }
      }
      this.setData({
        amount: amount,
        cabPreview: cabPreview
      })
    })
  },

  // 关闭预览图
  closeTap () {
    this.setData({
      showPreview: false
    })
  },

  // 打开预览图
  openTap () {
    this.setData({
      showPreview: true
    })
  },

  // 获取格口编号
  codeChange (e) {
    let value = e.detail.value;
    this.setData({
      codeValue: value
    })
  }, 

  // 获取列
  colChange(e) {
    let value = e.detail.value;
    this.setData({
      colValue: value
    })
  }, 

  // 获取行
  rowChange(e) {
    let value = e.detail.value;
    this.setData({
      rowValue: value
    })
  },

  // 查询
  search () {
    let boxNum = this.data.codeValue;
    let boxCol = this.data.colValue;
    let boxRow = this.data.rowValue;
    let tapValueId = this.data.cabInfo.id;
    if (!boxNum && !(boxCol && boxRow)) {
      wx.showToast({
        title: '请输入格口编号或行列',
        icon: 'none'
      })
      return;
    }
    let searchData = {
      cabId: tapValueId,
      boxNum: boxNum,
      boxSite: '',
      // boxSite: `${boxCol > 15 ? '' : '0'}${(+boxCol).toString(16)}${boxRow > 15 ? '' : '0'}${(+boxRow).toString(16)}`
      row:this.data.boxRow,
      column: this.data.boxCol
    }
    
    wx.navigateTo({
      url: '/pages/mouthManage/mouthManage?searchData=' + JSON.stringify(searchData),
    })
  },

  // 前往格口详情页
  toDetail (e) {
    let searchData = {
      cabId: this.data.cabInfo.id,
      boxNo: "",
      boxSite: e.currentTarget.dataset.boxcode,
      column: '',
      row: '',
    }
    wx.navigateTo({
      url: '/pages/mouthManage/mouthManage?searchData=' + JSON.stringify(searchData),
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})