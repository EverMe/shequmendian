// pages/priceManage/manage.js
import {
  config
} from '../../utils/config.js'
import {
  HTTP
} from '../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    allTypeBox: [{
        "image": '/image/icon@2x.png',
        "type": '大箱',
        "price": '0.40'
      },
      {
        "image": '/image/icon1@2x.png',
        "type": '中箱',
        "price": '0.40'
      },
      {
        "image": '/image/icon2@2x.png',
        "type": '小箱',
        "price": '0.40'
      },
      {
        "image": '/image/icon3@2x.png',
        "type": '微箱',
        "price": '0.40'
      },

    ],
    chestStatus: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    let nowChest
    http.request({ url: config.getcabinetinfo, data: { token: wx.getStorageSync('token') } }, (data) => {
      nowChest = data.data.list;
      this.setData({
        chestList: nowChest,
        defaultChest: nowChest[0].cabName,
        phone: options.phone, // 获取传递过来的phone
        selectChestId: nowChest[0].cabinet_id, //默认的柜子id
      })
      wx.setStorageSync('chestList', nowChest)
      this.getPrice()
    })
  
   
 
    
    
    
  },

  // 获取价格方法
  getPrice:function(){
    let params = {
      url: config.refreshprice,
      data: {
        token: wx.getStorageSync('token'),
        phone: this.data.phone,
        cabId: this.data.selectChestId
      }
    }
    http.request(params, (data) => {
      console.log(data)
      let numList = [];
      data.data.price_1 = (data.data.price_1 / 100).toFixed(2);
      data.data.price_2 = (data.data.price_2 / 100).toFixed(2);
      data.data.price_3 = (data.data.price_3 / 100).toFixed(2);
      data.data.price_4 = (data.data.price_4 / 100).toFixed(2);
      numList.push(data.data.price_1)
      numList.push(data.data.price_2)
      numList.push(data.data.price_3)
      numList.push(data.data.price_4)
      this.setData({
        numList: numList
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.getPrice()
  },

  toSetPrice: function(e) {
    let row = e.currentTarget.dataset.row + 1
    console.log(row)
    wx.navigateTo({
      url: '/pages/setPrice/set?cabId=' + this.data.selectChestId + '&row=' + row + '&phone='+this.data.phone,
    })
  },

  // 是否展开柜子列表
  chestStatus: function() {
    this.setData({
      chestStatus: !this.data.chestStatus,
    })
  },

  // 选择柜子
  selectChest: function(e) {
    let text = e.currentTarget.dataset.text;
    let otherChest = wx.getStorageSync('chestList');
    for (let i = 0; i < otherChest.length; i++) {
      if (text.cabName == otherChest[i].cabName) {
        otherChest.splice(i, 1)
      }
    }
    this.setData({
      defaultChest: text.cabName,
      chestStatus: false,
      chestList: otherChest,
      selectChestId: text.cabinet_id, //选择后的柜子id
    })
    console.log(this.data.selectChestId)
    this.getPrice()
  }
})