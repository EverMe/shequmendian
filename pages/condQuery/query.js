// pages/condQuery/query.js
import {
  config
} from '../../utils/config.js'
import {
  HTTP
} from '../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    reset: false,//重置按钮的显隐
    codeValue: '',//输入框的数据
    focus: true,//是否聚焦
    selectData: false,//是否显示下拉框数据
    tapValue:"",//下拉框选中的项
    allSelectData: "",//所有下拉框数据
    boxNum: "",//格口编号
    boxRow:"",//行数据
    boxCol:"",//列数据
    tapValueId: "",//选中柜机的id
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 请求所有柜机
    let params = {
      url: config.bindcabinetlist,
      data: {
        token: wx.getStorageSync('token')
      }
    }
    http.request(params,(res)=> {
      let data = res.data.list
      this.setData({
        allSelectData: data
      })
    })
  },

  // 搜索请求
  searchWaybill: function () {
    let codeValue = this.data.codeValue;
    wx.navigateTo({
      url: '/pages/searchWaybill/waybill?codeValue=' + codeValue,
    })
  },

  // 获取输入框内容
  getvalue: function(e) {
    let value = e.detail.value;
    this.setData({
      codeValue: value
    })
    if (!this.data.reset) {
      this.setData({
        reset: true,
      })
    }
    else if (value == '') {
      this.setData({
        reset: false,
      })
    }
  },

  // 扫码
  scanCode: function () {
    var that = this;
    wx.scanCode({
      success: (res) => {
        this.setData({
          searchText: true,
          codeValue: res.result,
          reset: true,
          waybillValue: res.result,
        })
        that.searchWaybill()
      }
    })
  },

  // 重置输入框内容
  reset: function () {
    this.setData({
      codeValue: '',
      reset: false,
      focus: true
    })
  },

  // 前往格口管理界面
  toMouthSearch: function() {
    let boxNum = this.data.boxNum;
    let boxCol = this.data.boxCol;
    let boxRow = this.data.boxRow;
    let tapValue = this.data.tapValue;
    let tapValueId = this.data.tapValueId;
    if (!tapValue) {
      wx.showToast({
        title: '请选择柜机！',
        icon:'none'
      })
      return;
    }
    if (!boxNum && !(boxCol && boxRow)) {
      wx.showToast({
        title: '请输入格口编号或行列',
        icon: 'none'
      })
      return;
    }
    let searchData = {
      cabId: tapValueId,
      boxNum: boxNum,
      // boxSite: `${boxCol > 15 ? '' : '0'}${(+boxCol).toString(16)}${boxRow > 15 ? '' : '0'}${(+boxRow).toString(16)}`
      boxSite: '',
      column: this.data.boxCol,
      row: this.data.boxRow,
    }
    wx.navigateTo({
      url: '/pages/mouthManage/mouthManage?searchData=' + JSON.stringify(searchData),
    })
  },

  // 监听格口编号
  boxNumChange: function(e) {
    let value = e.detail.value;
    this.setData({
      boxNum: value
    })
  },

  // 监听列变化
  boxColChange: function(e) {
    let value = e.detail.value;
    this.setData({
      boxCol: value
    })
  },

  // 监听行变化
  boxRowChange: function (e) {
    let value = e.detail.value;
    this.setData({
      boxRow: value
    })
  },

  // 选择柜机
  selectCabinet(e) {
    let value = e.detail.value;
    let allSelectData = this.data.allSelectData;
    this.setData({
      tapValueId: allSelectData[value].id,
      tapValue: allSelectData[value].cabName,
      tapItem: allSelectData[value]
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})