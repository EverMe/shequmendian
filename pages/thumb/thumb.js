// pages/home/home.js
import { config } from '../../utils/config.js'
import { HTTP } from '../../utils/http.js';
const http = new HTTP();
const app = getApp();
Page({


  data: {
    current: 0,//当前swiper页数
    dropDown: ['柜子B'],//可选的柜子
    chestStudus: false,//是否弹出选择柜子框
    chest: '',
    duration: 200,//swiper滑动时长
  },


  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
      mask:true
    })

    // 登录
      wx.login({
        success: res => {
          if (res.code) {
            var params = {
              url: config.login,
              data: {
                jsCode: res.code
              }
            };
            http.request(params, (res) => {
              console.log(res)
              if (res.data && res.data.info.Phone && res.data.info.Phone != '0') {
                wx.setStorageSync('info', res.data.info)
                wx.setStorageSync('token', res.data.info.token)
                this.LoginLater()
              } else {
                wx.setStorageSync('token', res.data.info.token)
                wx.redirectTo({
                  url: '/pages/Reg/Reg'
                })
              }
            });
          } else {
            wx.showModal({
              content: '获取用户登录态失败！' + res.errMsg
            })
          }
        }
      });
    
    
  },

  LoginLater:function() {
    http.request({ url: config.getcabinet, data: { token: wx.getStorageSync('token') } }, (data) => {

      if(!data.data.list[0]) {
        wx.showToast({
          title: '您目前还没有柜子！',
          icon:'none'
        })
        this.setData({
          chest:'暂无柜子',
          swiperitemList:['1']
        })
        return;
      }

      wx.setStorageSync('chestList', data.data.list)

      let nowChest = wx.getStorageSync('chestList')
      nowChest.splice(0, 1);

      this.setData({
        chest: data.data.list[0].expressCabinets.cabName,
        chestId: data.data.list[0].expressCabinets.terminalCode,
        dropDown: nowChest
      })

      // 请求柜子数据
      let params = {
        url: config.getcabinetgrid,
        data: {
          token: wx.getStorageSync('token'),
          terminalCode: this.data.chestId
        }
      }
      http.request(params, (data) => {
        wx.hideLoading()
        let chestNum = data.data.row;
        let arrayList = Math.ceil(chestNum / 24);
        let swiperitemList = []
        for (let i = 0; i < arrayList; i++) {
          swiperitemList[i] = []
        }
        this.setData({
          swiperitemList: swiperitemList,
          newChestList: data.data.list,
        })

      })
    })
  },



  onShow: function () {

  },

  // 切换为下一页
  addCurrent: function () {
    this.setData({
      current: this.data.current + 1,
    })
  },

  // 切换为上一页
  minusCurrent: function () {
    this.setData({
      current: this.data.current - 1,
    })
  },

  // 选择柜子，弹出/收起下拉框
  selectChest: function () {
    this.setData({
      chestStudus: !this.data.chestStudus
    })
  },

  // 选中柜子操作
  selected: function (e) {
    let text = e.currentTarget.dataset.text;
    let chestId = e.currentTarget.dataset.chestid;
    let otherChest = wx.getStorageSync('chestList');
    for (let i = 0; i < otherChest.length; i++) {
      if (text == otherChest[i].expressCabinets.cabName) {
        otherChest.splice(i, 1)
      }
    }

    this.setData({
      chest: text,
      chestStudus: false,
      chestId: chestId,
      current:0,
      dropDown: otherChest,
    })

    // 请求柜子数据
    let params = {
      url: config.getcabinetgrid,
      data: {
        token: wx.getStorageSync('token'),
        terminalCode: this.data.chestId
      }
    }
    http.request(params, (data) => {

      let chestNum = data.data.row;
      let arrayList = Math.ceil(chestNum / 24);
      let swiperitemList = []
      for (let i = 0; i < arrayList; i++) {
        swiperitemList[i] = []
      }
      this.setData({
        swiperitemList: swiperitemList,
        newChestList: data.data.list,
      })
     
    })
    
  },

  // 监听swiper的滑动事件
  swiperChange: function (e) {
    this.setData({
      current: e.detail.current
    })
  },

  onPullDownRefresh: function () {
    this.onLoad()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },
})
