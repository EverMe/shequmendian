// pages/priceSetting/setting.js
import {
  config
} from '../../../utils/config.js'
import {
  HTTP
} from '../../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tapValue: "全部",//选中的柜机名称
    tapItem: "",//选中项的所有数据
    cabList:'',//柜子数据
    allSelectData:"",//所有下拉框数据
    memInfo:"",//快递员信息
    selectCablist: [],//选中的柜机
    index: 0,//选中柜机的index
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 请求所有柜机
    let allSelectData = [{ cabName: '全部',id: ''}]
    let params = {
      url: config.bindcabinetlist,
      data: {
        token: wx.getStorageSync('token')
      }
    }
    http.request(params, (res) => {
      let data = res.data.list
      data = allSelectData.concat(data)
      this.setData({
        allSelectData: data
      })
    })
  },

  onShow () {
    let params = {
      url: config.getpricelist,
      data: {
        token: wx.getStorageSync('token')
      }
    }
    http.request(params, (res) => {
      let data = res.data.list;
      let index = this.data.index;
      
      this.setData({
        cabList: data,
      })
      if (index !== 0) {
        this.selectCabinet()
      }
      else {
        this.setData({
          selectCablist: data
        })
      }
      
    })
  },

  // 选择柜机
  selectCabinet(e) {
    let value = e && e.detail.value || this.data.index;
    let allSelectData = this.data.allSelectData;
    let id = allSelectData[value].id;
    let cabList = this.data.cabList;
    let selectCablist = [];
    if (value != 0) {
      for (let i = 0; i < cabList.length; i++) {
        if (cabList[i].cabId == id) {
          selectCablist.push(cabList[i])
          this.setData({
            tapValue: allSelectData[value].cabName,
            selectCablist: selectCablist,
            index: value
          })
        }
      }
      
    }
    else {
      this.setData({
        tapValue: allSelectData[value].cabName,
        selectCablist: cabList
      })
    }
    
  },

  // 跳转多柜设置免费快递员
  toCabFreeMem: function() {
    // let cabInfo = this.data.tapItem;
    // if (!cabInfo) {
    //   wx.showToast({
    //     title: '请先选择柜机',
    //     icon: 'none'
    //   })
    //   return;
    // }
    wx.navigateTo({
      url: '/pages/priceSetting/cabFreeMem/cabFreeMem',
    })
  },

  // 价格设置
  toPrice: function (e) {
    let cabInfo = e.currentTarget.dataset.item;
    wx.navigateTo({
      url: '/pages/priceSetting/price/price?cabInfo=' + JSON.stringify(cabInfo),
    })
  },
})