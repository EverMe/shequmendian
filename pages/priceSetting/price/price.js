// pages/priceSetting/price/price.js
import {
  config
} from '../../../utils/config.js'
import {
  HTTP
} from '../../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    price: "",//当前格口价格
    cabId: "",//柜机id
    members: "",//快递员列表
    price_3: "",//大格口价格
    price_2: "",//中格口价格
    price_1: "",//小格口价格
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let cabInfo = JSON.parse(options.cabInfo);
    let cabId = cabInfo.cabId;
    this.setData({
      price_1: cabInfo.price1/100,
      price_2: cabInfo.price2/100,
      price_3: cabInfo.price3/100,
      cabId: cabId,
      cabInfo: cabInfo
    })
    
  },

  onShow () {
    this.getFreeMem()
  },

  getFreeMem () {
    let params = {
      url: config.getwhitelist,
      data: {
        token: wx.getStorageSync('token'),
        cabId: this.data.cabId
      }
    }
    // 获取免费快递员白名单
    http.request(params, (res) => {
      this.setData({
        members: res.data.list
      })
    })
  },

  // 设置免费快递员
  toFreeMem: function(e) {
    let cabInfo = this.data.cabInfo;
    let meminfo = e.currentTarget.dataset.meminfo;
    wx.navigateTo({
      url: '/pages/priceSetting/freeMem/freeMem?cabInfo=' + JSON.stringify(cabInfo) + '&meminfo=' + JSON.stringify(meminfo),
    })
  },

  // 获取大格口价格
  bpChange (e) {
    let value = e.detail.value;
    console.log(value)
    this.setData({
      price_3: value
    })
  },

  // 获取中格口价格
  mpChange(e) {
    let value = e.detail.value;
    this.setData({
      price_2: value
    })
  },

  // 获取小格口价格
  spChange(e) {
    let value = e.detail.value;
    this.setData({
      price_1: value
    })
  },

  // 确定提交价格
  submitPrice () {
    let price_1 = this.data.price_1;
    let price_2 = this.data.price_2;
    let price_3 = this.data.price_3;
    if (!price_1 && !price_2 && !price_3) {
      wx.showToast({
        title: '请将价格填写完整',
        icon: 'none'
      })
    }
    let params = {
      url: config.setprice,
      data: {
        token: wx.getStorageSync('token'),
        cabId: this.data.cabId,
        updPrice_1: price_1 * 100,
        updPrice_2: price_2 * 100,
        updPrice_3: price_3 * 100,
      }
    }
    http.request(params,(res)=> {
      wx.showToast({
        title: '价格设置成功',
      })
    })
  },
  
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})