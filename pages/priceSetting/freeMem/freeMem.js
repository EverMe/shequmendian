// pages/priceSetting/freeMem/freeMem.js
import {
  config
} from '../../../utils/config.js'
import {
  HTTP
} from '../../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cabInfo: "",//柜机信息
    memPhone: "",//快递员账号
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let cabInfo = JSON.parse(options.cabInfo)
    let meminfo = JSON.parse(options.meminfo)
    this.setData({
      cabInfo: cabInfo,
      meminfo: meminfo
    })
  },

  // 获取输入框快递员账号
  memChange (e) {
    let value = e.detail.value;
    this.setData({
      memPhone: value
    })
  },

  // 重置快递员账号
  reset () {
    this.setData({
      memPhone: ""
    })
  },

  // 查询免费快递员
  memSearch () {
    let params = {
      url: config.searchwhitelistall,
      data: {
        token: wx.getStorageSync('token'),
        cabId: this.data.cabInfo.cabId,
        phone: this.data.memPhone
      }
    }
    http.request(params,(res)=> {
      this.setData({
        meminfo: res.data.expressMember
      })
    })
  },

  // 设置免费
  setFree () {
    let params = {
      url: config.setwhitelistone,
      data: {
        token: wx.getStorageSync('token'),
        cabId: this.data.cabInfo.cabId,
        memberId: this.data.meminfo.memberId,
        cmd: 'JOIN'
      }
    }
    http.request(params,(res)=> {
      wx.showToast({
        title: '设置成功',
      })
    })
  },

  // 取消免费
  cancleFree() {
    let params = {
      url: config.setwhitelistone,
      data: {
        token: wx.getStorageSync('token'),
        cabId: this.data.cabInfo.cabId,
        memberId: this.data.meminfo.memberId,
        cmd: 'CANCEL'
      }
    }
    http.request(params, (res) => {
      wx.showToast({
        title: '取消成功',
      })
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})