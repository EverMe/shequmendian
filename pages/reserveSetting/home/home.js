// pages/reserveSetting/home/home.js
import {
  config
} from '../../../utils/config.js'
import {
  HTTP
} from '../../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tapValue: "全部",//选中的柜机名称
    tapItem: "",//选中项的所有数据
    cabList: '',//柜子数据
    allSelectData: "",//所有下拉框数据
    memInfo: "",//快递员信息
    selectCablist: [],//选中的柜机
    index: 0,//默认选中的柜机
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 请求所有柜机
    let allSelectData = [{ cabName: '全部', id: '' }]
    let params = {
      url: config.bindcabinetlist,
      data: {
        token: wx.getStorageSync('token')
      }
    }
    http.request(params, (res) => {
      let data = res.data.list
      data = allSelectData.concat(data)
      this.setData({
        allSelectData: data
      })
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let params = {
      url: config.operatbookinglist,
      data: {
        token: wx.getStorageSync('token')
      }
    }
    http.request(params, (res) => {
      let data = res.data.list;
      let index = this.data.index;

      this.setData({
        cabList: data,
      })
      if (index !== 0) {
        this.selectCabinet()
      }
      else {
        this.setData({
          selectCablist: data
        })
      }

    })
  },

  // 选择柜机
  selectCabinet(e) {
    let value = e && e.detail.value || this.data.index;
    let allSelectData = this.data.allSelectData;
    let id = allSelectData[value].id;
    let cabList = this.data.cabList;
    let selectCablist = [];
    if (value != 0) {
      for (let i = 0; i < cabList.length; i++) {
        if (cabList[i].cabId == id) {
          selectCablist.push(cabList[i])
          this.setData({
            tapValue: allSelectData[value].cabName,
            selectCablist: selectCablist,
            index: value
          })
        }
      }

    }
    else {
      this.setData({
        tapValue: allSelectData[value].cabName,
        selectCablist: cabList
      })
    }

  },
  
  // 跳转多个柜子预留快递员设置
  toReserveMems: function() {
    wx.navigateTo({
      url: '/pages/reserveSetting/reserveMems/reserveMems',
    })
  },

  // 跳转单个柜子预留快递员设置
  toReserveMouth: function (e) {
    let cabInfo = e.currentTarget.dataset.item;
    wx.navigateTo({
      url: '/pages/reserveSetting/reserveMouth/reserveMouth?cabInfo=' + JSON.stringify(cabInfo),
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})