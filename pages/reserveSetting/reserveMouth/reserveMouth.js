import {
  config
} from '../../../utils/config.js'
import {
  HTTP
} from '../../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    price: "",//当前格口价格
    cabId: "",//柜机id
    members: "",//快递员列表
    largeBK: "",//大格口剩余格口数
    largeTOT: "",//大格口总数
    middleBK: "",//中格口剩余格口数
    middleTOT: "",//中格口总数
    smallBK: "",//小格口剩余格口数
    smallTOT: "",//小格口总数
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let cabInfo = JSON.parse(options.cabInfo);
    let cabId = cabInfo.cabId;
    this.setData({
      cabId: cabId,
      cabInfo: cabInfo,
      largeBK: cabInfo.largeBK,//大格口剩余格口数
      largeTOT: cabInfo.largeTOT,//大格口总数
      middleBK: cabInfo.middleBK,//中格口剩余格口数
      middleTOT: cabInfo.middleTOT,//中格口总数
      smallBK: cabInfo.smallBK,//小格口剩余格口数
      smallTOT: cabInfo.smallTOT,//小格口总数
    })
  },

  onShow() {
    let params = {
      url: config.getreservewhitelist,
      data: {
        token: wx.getStorageSync('token'),
        cabId: this.data.cabId
      }
    }
    // 获取免费快递员白名单
    http.request(params, (res) => {
      this.setData({
        members: res.data.list
      })
    })
  },

  // 设置免费快递员
  toFreeMem: function (e) {
    let cabInfo = this.data.cabInfo;
    let meminfo = e.currentTarget.dataset.meminfo;
    wx.navigateTo({
      url: '/pages/reserveSetting/reserveMem/reserveMem?cabInfo=' + JSON.stringify(cabInfo) + '&meminfo=' + JSON.stringify(meminfo),
    })
  },

  // 获取大格口预留数
  bpChange(e) {
    let value = e.detail.value;
    console.log(value)
    this.setData({
      largeBK: value
    })
  },

  // 获取中格口预留数
  mpChange(e) {
    let value = e.detail.value;
    this.setData({
      middleBK: value
    })
  },

  // 获取小格口预留数
  spChange(e) {
    let value = e.detail.value;
    this.setData({
      smallBK: value
    })
  },

  // 确定提交价格
  submitPrice() {
    let largeBK = this.data.largeBK;
    let middleBK = this.data.middleBK;
    let smallBK = this.data.smallBK;
    if (!smallBK && !middleBK && !largeBK) {
      wx.showToast({
        title: '请将预留数填写完整',
        icon: 'none'
      })
    }
    let params = {
      url: config.setoperatbookingamount,
      data: {
        token: wx.getStorageSync('token'),
        cabId: this.data.cabId,
        largeUpd: largeBK,
        middleUpd: middleBK,
        smallUpd: smallBK,
      }
    }
    http.request(params, (res) => {
      wx.showToast({
        title: '预留设置成功',
      })
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})