import {
  config
} from '../../../utils/config.js'
import {
  HTTP
} from '../../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cabInfo: "", //柜机信息
    memInfo: "", //快递员信息
    memPhone: "", //搜索框快递员账号
    allCabinet: "", //所有绑定柜机信息
    checkedAll: false, //是否全选
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let cabInfo = JSON.parse(options.cabInfo)
    let memInfo = JSON.parse(options.meminfo)
    this.setData({
      cabInfo: cabInfo,
      memInfo: memInfo
    })

    // 获取所有柜子信息
    let params = {
      url: config.bindcabinetlist,
      data: {
        token: wx.getStorageSync('token')
      }
    }
    http.request(params, (res) => {
      let data = res.data.list;
      for (let i = 0; i < data.length; i++) {
        data[i]['checked'] = false
      }
      this.setData({
        allCabinet: data
      })
    })
  },

  // 设置/取消 预留
  isAccredit(e) {
    let operate;
    let value = e.currentTarget.dataset.operate;
    if (value === 'set') {
      operate = 'JOIN'
    } else {
      operate = 'CANCEL'
    }
    let memInfo = this.data.memInfo;
    if (!memInfo) {
      wx.showToast({
        title: '请查询快递员后设置',
        icon: 'none'
      })
      return;
    }
    let params = {
      url: config.setoperatbookingone,
      data: {
        token: wx.getStorageSync("token"),
        memberId: this.data.memInfo.memberId,
        cabId: this.data.cabInfo.cabId,
        cmd: operate
      }
    }
    if (operate === 'JOIN') {
      http.request(params, (res) => {
        wx.showToast({
          title: '设置成功',
        })
      })
    } else {
      http.request(params, (res) => {
        wx.showToast({
          title: '取消成功',
        })
      })
    }

  },

  // 获取输入框内容
  memChange(e) {
    let value = e.detail.value;
    this.setData({
      memPhone: value
    })
  },

  // 清空输入框
  reset() {
    this.setData({
      memPhone: ""
    })
  },

  // 搜索快递员
  memSearch() {
    let params = {
      url: config.searchwhitelistall,
      data: {
        token: wx.getStorageSync('token'),
        cabId: this.data.cabInfo.id,
        phone: this.data.memPhone
      }
    }
    http.request(params, (res) => {
      if (!res.data.expressMember) {
        wx.showToast({
          title: '暂无此快递员',
        })
      }
      this.setData({
        memInfo: res.data.expressMember
      })
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})