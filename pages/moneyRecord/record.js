// pages/moneyRecord/record.js
import { config } from '../../utils/config.js'
import { HTTP } from '../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    monthList:[1,2,3,4,5,6,7,8,9,10,11,12],
    dayList: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let d = new Date();
    let day = d.getDate();
    let month = d.getMonth()+1;
    let year = d.getFullYear();
    this.setData({
      day:day,
      month: month,
      year: year,
      dateMonth: `${year}-${month}`
    })
    this.moneyDetail()
    wx.setStorageSync('dayList', this.data.dayList)
    this.dayList()
    // 存储当前月份天数
    this.setData({
      dayNum:this.data.dayList.length,
    })
  },

  // 请求收益明细数据
  moneyDetail:function() {
    let params = {
      url: config.getbossincome,
      data: {
        token: wx.getStorageSync('token'),
        dateMonth: this.data.dateMonth
      }
    }
    http.request(params, (data) => {
      this.setData({
        dayRecord: data.data.list,
        moneyRecord: data.data.record,
      })
    })
  },

  // 选择时间
  bindDateChange: function(e) {
    console.log(e.detail.value)
    let date = e.detail.value;
    this.setData({
      year: +date.slice(0, 4),
      dateMonth: `${+date.slice(0, 4)}-${+this.data.month}`
    })
    this.moneyDetail()
    this.dayList()
    this.setData({
      dayNum: this.data.dayList.length,
    })
  },

  // 月份选择
  selectMonth: function(e) {
    // 存储当前选择月份
    this.setData({
      month: this.data.monthList[e.detail.value]
    })
    // 获取当月天数
    this.dayList()
    // 存储天数最后一日并存储年月参数
    this.setData({
      day: this.data.dayList[this.data.dayList.length-1],
      dateMonth:`${this.data.year}-${this.data.month}`
    })
    // 请求当前时间收益数据
    this.moneyDetail()
    // 数据置顶
    this.setData({
      toTop:0
    })
    this.setData({
      dayNum: this.data.dayList.length,
    })
  },

  // 选择日
  selectDay: function(e) {
    let index = e.detail.value;
    let scorllNum = parseInt (index) + 1;
    this.setData({
      day:this.data.dayList[index],
      scrollId: 'position' + scorllNum
    })
    
  },

  // 判断有几日
  dayList:function() {
    let year = this.data.year;
    let month = this.data.month;
    if (year % 4 == 0 && month == 2) {
      let newList = wx.getStorageSync('dayList')
      newList.splice(29, 4)
      this.setData({
        dayList: newList
      })
    }
    else {
      if(month == 2) {
        let newList = wx.getStorageSync('dayList')
        newList.splice(28, 4)
        this.setData({
          dayList: newList
        })
      }
      else if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
        this.setData({
          dayList: wx.getStorageSync('dayList')
        })
      }

      else {
        let newList = wx.getStorageSync('dayList')
        newList.splice(30, 1)
        this.setData({
          dayList: newList
        })
      }
    }
  },

  // 根据滚轮位置变化日期
  changeDay: function(e) {
    let scrollTop = e.detail.scrollTop;
    // 增加条件，避免频繁setData影响性能
    if (this.data.day != this.data.dayNum - parseInt(scrollTop / 60) && scrollTop > 0) {
      this.setData({
        day: this.data.dayNum - parseInt(scrollTop / 60),
      })
    }
  },

  // 跳转每日详细流水
  toDayMoney: function(e) {
    let date = e.currentTarget.dataset.date;
    wx.navigateTo({
      url: '/pages/dayMoney/money?date=' + date,
    })
  }
})