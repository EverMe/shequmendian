// pages/userInfo/userInfo.js
import { config } from '../../utils/config.js'
import { HTTP } from '../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  onShow: function() {
    let params = {
      url: config.getinfo,
      data: {
        token: wx.getStorageSync('token')
      }
    }
    http.request(params, (res) => {
      let data = res.data.info;
      data['tail'] = data.bankCard ? data.bankCard.slice(-4) : '';
      this.setData({
        userInfo: res.data.info,
        isPrimary: res.data.isPrimary
      })
    })
  },

  // 跳转银行卡设置
  toBankcard () {
    if (!this.data.isPrimary) {
      return;
    }
    wx.navigateTo({
      url: '/pages/vallet/bankCard/card',
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})