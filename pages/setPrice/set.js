// pages/setPrice/set.js
import {
  config
} from '../../utils/config.js'
import {
  HTTP
} from '../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      cabId: options.cabId,
      row: options.row,
      phone:options.phone
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  // 获取输入框的值

  getValue: function(e) {
    let value = e.detail.value;
    this.setData({
      keyValue: value,
    })
  },

// 确认提交
  submit: function() {
    if(this.data.keyValue>10) {
      wx.showToast({
        title: '请将价格调到合理范围!',
        icon:'none',
        duration:2000
      })
      return;
    }
  let params = {
    url: config.setcourier,
    data:{
      token:wx.getStorageSync('token'),
      phone:this.data.phone,
      'type':'0',
      cabId: this.data.cabId,
      row: this.data.row,
      price: (this.data.keyValue*100).toString()
    }
  }
    http.request(params,(data)=>{
      wx.navigateBack({
        delta: 1,
        success:function() {
          wx.showToast({
            title: '修改成功',
          })
        }
      })
    })
  },
})