// pages/overdueSetting/oneCabinet/oneCabinet.js
import {
  config
} from '../../../utils/config.js'
import {
  HTTP
} from '../../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cabInfo: "",//柜机信息
    // 逾期时限下拉框
    forTime: [24, 48, 72],//可选的时限
    timeLimit: 24,//选中的时限
    overdueControl: "",//选中的控制类型
    selectControl: ['处理后允许投递', '仅提醒，不限制'],
    checkedAll: false,//是否全选
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let cabInfo = JSON.parse(options.cabInfo)
    this.setData({
      cabInfo: cabInfo
    })

    this.setData({
      timeLimit: +cabInfo.overdueValCN.slice(0, 2),
      overdueControl: cabInfo.overdueOnCN
    })

    // 获取所有柜子信息
    let params = {
      url: config.bindcabinetlist,
      data: {
        token: wx.getStorageSync('token')
      }
    }
    http.request(params, (res) => {
      let data = res.data.list;
      for (let i = 0; i < data.length; i++) {
        data[i]['checked'] = false
      }
      this.setData({
        allCabinet: data
      })
    })
  },

  // 选择时限
  timePickerChange: function (e) {
    let index = e.detail.value
    this.setData({
      timeLimit: this.data.forTime[index]
    })
  },

  // 逾期控制
  conbindPickerChange(e) {
    let index = e.detail.value
    this.setData({
      overdueControl: this.data.selectControl[index]
    })
  },

  // 确定设置
  sureSet() {
    let overdueControl = this.data.overdueControl;

    if (!overdueControl) {
      wx.showToast({
        title: '请选择逾期控制',
        icon: 'none'
      })
      return;
    }
    let cmd;
    if (overdueControl === '处理后允许投递') {
      cmd = 'NEED'
    }
    else {
      cmd = 'DEFAULT'
    }
    let params = {
      url: config.setoperatoverdueone,
      data: {
        token: wx.getStorageSync("token"),
        cabId: this.data.cabInfo.id,
        due: this.data.timeLimit,
        cmd: cmd
      }
    }
    http.request(params, (res) => {
      wx.showToast({
        title: '设置成功',
      })
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})