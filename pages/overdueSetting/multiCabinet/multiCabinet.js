// pages/overdueSetting/multiCabinet/multiCabinet.js
import {
  config
} from '../../../utils/config.js'
import {
  HTTP
} from '../../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cabInfo: "",//柜机信息
    // 逾期时限下拉框
    forTime: [24,48,72],//可选的时限
    timeLimit: 24,//选中的时限
    overdueControl:"",//选中的控制类型
    selectControl: ['处理后允许投递','仅提醒，不限制'],
    checkedAll: false,//是否全选
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // let cabInfo = JSON.parse(options.cabInfo)
    // this.setData({
    //   cabInfo: cabInfo
    // })

    // 获取所有柜子信息
    let params = {
      url: config.bindcabinetlist,
      data: {
        token: wx.getStorageSync('token')
      }
    }
    http.request(params, (res) => {
      let data = res.data.list;
      for (let i = 0; i < data.length; i++) {
        data[i]['checked'] = false
      }
      this.setData({
        allCabinet: data
      })
    })
  },

  // 选择时限
  timePickerChange: function (e) {
    let index = e.detail.value
    this.setData({
      timeLimit: this.data.forTime[index]
    })
  },

  // 逾期控制
  conbindPickerChange (e) {
    let index = e.detail.value
    this.setData({
      overdueControl: this.data.selectControl[index]
    })
  },

  // 选择某个柜机
  cabChecked(e) {
    let index = e.currentTarget.dataset.index;
    let allCabinet = this.data.allCabinet;
    for (let i = 0; i < allCabinet.length; i++) {
      if (i === index) {
        allCabinet[i]['checked'] = !allCabinet[i]['checked']
      }
    }
    this.setData({
      allCabinet: allCabinet
    })
  },

  // 选择所有柜机
  allChecked() {
    let allCabinet = this.data.allCabinet;
    let checkedAll = this.data.checkedAll;
    if (checkedAll) {
      for (let i = 0; i < allCabinet.length; i++) {
        allCabinet[i]['checked'] = false
      }
    }
    else {
      for (let i = 0; i < allCabinet.length; i++) {
        allCabinet[i]['checked'] = true
      }
    }
    this.setData({
      allCabinet: allCabinet,
      checkedAll: !this.data.checkedAll
    })
  },

  // 确定设置
  sureSet() {
    let overdueControl = this.data.overdueControl;
    let cabIds = [];
    let allCabinet = this.data.allCabinet;
    for (let i = 0; i < allCabinet.length; i++) {
      if (allCabinet[i].checked === true) {
        cabIds.push(allCabinet[i].id)
      }
    }
    if (!overdueControl) {
      wx.showToast({
        title: '请选择逾期控制',
        icon: 'none'
      })
      return;
    }
    if (!cabIds[0]) {
      wx.showToast({
        title: '请至少勾选一组柜子',
        icon: 'none'
      })
      return;
    }
    let cmd;
    if (overdueControl === '处理后允许投递') {
      cmd = 'NEED'
    }
    else {
      cmd = 'DEFAULT'
    }
    let params = {
      url: config.setoperatoverdueall,
      data: {
        token: wx.getStorageSync("token"),
        cabIds: JSON.stringify(cabIds),
        due: this.data.timeLimit,
        cmd: cmd
      }
    }
    http.request(params, (res) => {
      wx.showToast({
        title: '设置成功',
      })
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})