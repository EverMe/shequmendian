// pages/myCabinet/home/home.js
import {
  config
} from '../../../utils/config.js'
import {
  HTTP
} from '../../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cabList: "",//预留柜机列表
    tapValue: "全部",//选中的柜机名称
    tapItem: "",//选中项的所有数据
    allSelectData: "",//所有下拉框数据
    selectCablist: [],//选中的柜机
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  onShow() {
    // 请求所有柜机
    let params = {
      url: config.bindcabinetlist,
      data: {
        token: wx.getStorageSync('token')
      }
    }
    http.request(params, (res) => {
      let data = res.data.list;
      let allSelectData = [{cabName: '全部', id: 0}];
      allSelectData = allSelectData.concat(data)
      this.setData({
        allSelectData: allSelectData,
        cabList: data,
        selectCablist: data,
        tapValue: '全部'
      })
    })
  },

  // 选择柜机
  selectCabinet(e) {
    let value = e.detail.value;
    let allSelectData = this.data.allSelectData;
    let cabList = this.data.cabList;
    let selectCablist = [];
    if (value == 0) {
      selectCablist = cabList
    }
    else {
      selectCablist[0] = cabList[value - 1]
    }
    this.setData({
      tapValue: allSelectData[value].cabName,
      selectCablist: selectCablist
    })
  },

  // 跳转新增柜机
  toBinding: function () {
    let cabInfo = this.data.tapItem;
    // if (!cabInfo) {
    //   wx.showToast({
    //     title: '请先选择柜机',
    //     icon: 'none'
    //   })
    //   return;
    // }
    wx.navigateTo({
      url: '/pages/myCabinet/binding/binding?cabInfo=' + JSON.stringify(cabInfo),
    })
  },

  // 跳转柜机详情
  toCabDetail: function (e) {
    let cabInfo = e.currentTarget.dataset.item;
    wx.navigateTo({
      url: '/pages/myCabinet/cabDetail/detail?cabInfo=' + JSON.stringify(cabInfo),
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})