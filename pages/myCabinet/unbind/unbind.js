// pages/myCabinet/unbind/unbind.js
import {
  config
} from '../../../utils/config.js'
import {
  HTTP
} from '../../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    codeValue: '发送验证码',
    code: '',//验证码
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let cabInfo = JSON.parse(options.cabInfo);
    this.setData({
      cabInfo: cabInfo
    })
  },

  // 发送验证码
  sendCode() {
    let countDown = 60;
    if (this.data.codeValue != '发送验证码') {
      return;
    }
    let params = {
      url: config.sendmsg,
      data: {
        token: wx.getStorageSync('token'),
        type: 1,
        cabSysId: this.data.cabInfo.cabSysId,
        machineCode: this.data.cabInfo.machineCode
      }
    }
    http.request(params, (res) => {
      wx.showToast({
        title: '验证码已发送',
        icon: 'none'
      })
    })
    this.setData({
      codeValue: countDown
    })
    this.wite(countDown)
  },

  wite(countDown) {
    this.setData({
      codeValue: countDown + 's'
    })
    countDown -= 1;
    if (countDown > -1) {
      setTimeout(() => {
        this.wite(countDown)
      }, 1000)
    } else {
      this.setData({
        codeValue: '发送验证码'
      })
    }
  },

  // 监听验证码
  codeChange (e) {
    let code = e.detail.value;
    this.setData({
      code: code
    })
  },  

  // 解绑
  unbind () {
    let params = {
      url: config.cancelcabinet,
      data: {
        token: wx.getStorageSync('token'),
        code: this.data.code,
        cabId: this.data.cabInfo.id
      }
    }
    http.request(params,(res)=> {
      wx.showToast({
        title: '解绑成功',
        mask: true
      })
      setTimeout(()=> {
        wx.navigateBack({
          delta: 2
        })
      },1500)
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})