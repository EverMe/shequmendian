// pages/myCabinet/cabDetail/detail.js
import {
  config
} from '../../../utils/config.js'
import {
  HTTP
} from '../../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let cabInfo = JSON.parse(options.cabInfo);
    // 查询柜机详情
    let cabId = cabInfo.id;
    let params = {
      url: config.bindcabinetdetail,
      data: {
        token: wx.getStorageSync('token'),
        cabId: cabId
      }
    }
    http.request(params,(res)=> {
      this.setData({
        cabDetail: res.data.cabinetInfo
      })
    })
  },

  
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  // 解除绑定
  unbind (e) {
    let cabInfo = e.currentTarget.dataset.item;
    wx.navigateTo({
      url: '/pages/myCabinet/unbind/unbind?cabInfo=' + JSON.stringify(cabInfo),
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})