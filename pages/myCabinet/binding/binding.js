// pages/myCabinet/binding/binding.js
import {
  config
} from '../../../utils/config.js'
import {
  HTTP
} from '../../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    code: '',//验证码
    terminalCode: '',//网点编号
    machineCode: '',//主机序列号
    codeValue: '发送验证码',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  // 发送验证码
  sendCode() {
    let countDown = 60;
    if (!this.data.terminalCode) {
      wx.showToast({
        title: '请输入网点编号',
        icon: 'none'
      })
      return;
    }

    if (!this.data.machineCode) {
      wx.showToast({
        title: '请输入主机硬件序列号',
        icon: 'none'
      })
      return;
    }
    if (this.data.codeValue != '发送验证码') {
      return;
    }
    let params = {
      url: config.sendmsg,
      data: {
        token: wx.getStorageSync('token'),
        type: 1,
        cabSysId: this.data.terminalCode,
        machineCode: this.data.machineCode
      }
    }
    http.request(params,(res)=> {
      wx.showToast({
        title: '验证码已发送',
        icon: 'none'
      })
    })
    this.setData({
      codeValue: countDown
    })
    this.wite(countDown)
  },

  wite(countDown) {
    this.setData({
      codeValue: countDown + 's'
    })
    countDown -= 1;
    if (countDown > -1) {
      setTimeout(() => {
        this.wite(countDown)
      }, 1000)
    } else {
      this.setData({
        codeValue: '发送验证码'
      })
    }
  },

  // 监听验证码
  codeChange (e) {
    let value = e.detail.value;
    this.setData({
      code: value
    })
  },

  // 监听网点编号
  termChange(e) {
    let value = e.detail.value;
    this.setData({
      terminalCode: value
    })
  },

  // 监听主机硬件序列号
  machineChange(e) {
    let value = e.detail.value;
    this.setData({
      machineCode: value
    })
  },

  // 绑定
  binging () {
    let code = this.data.code;
    let terminalCode = this.data.terminalCode;
    let machineCode = this.data.machineCode;
    let cabSysId = this.data.terminalCode
    if (!cabSysId) {
      wx.showToast({
        title: '请输入网点编号',
        icon: 'none'
      })
    }else if (!machineCode) {
      wx.showToast({
        title: '请输入主机硬件序列号',
        icon: 'none'
      })
    } else if (!code) {
      wx.showToast({
        title: '请输入验证码',
        icon: 'none'
      })
    }else {
      let params = {
        url: config.setcabinet,
        data: {
          token: wx.getStorageSync('token'),
          code: code,
          machineCode: machineCode,
          cabSysId: cabSysId
        }
      }
      http.request(params,(res)=> {
        wx.showToast({
          title: '绑定成功',
          mask: true
        })
        setTimeout(()=> {
          wx.navigateBack({
            
          })
        },1000)
      })
    }
  },

  // 拨打电话
  call () {
    wx.makePhoneCall({
      phoneNumber: '400-6066-236'
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})