// pages/courierManage/manage.js
import {
  config
} from '../../utils/config.js'
import {
  HTTP
} from '../../utils/http.js';
const http = new HTTP();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    noData: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    let params = {
      url: config.courierlist,
      data: {
        token: wx.getStorageSync('token'),
      }
    }
    http.request(params, (data) => {
      console.log(data)
      if (!data.data.list[0]) {
        this.setData({
          noData: true
        })
      }
      this.setData({
        courierList: data.data.list
      })
    })
  },

  toSetAllBox: function(e) {
    let phone = e.currentTarget.dataset.phone;
    wx.navigateTo({
      url: '/pages/priceManage/manage?phone='+phone,
    })
  },

  // 获取输入的值
  keyValue: function(e) {
    let value = e.detail.value;
    this.setData({
      keyValue: value.toString()
    })
    console.log(this.data.keyValue)
  },

  // 开始查找相应数据
  search: function() {
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    let params = {
      url: config.courierlist,
      data: {
        token: wx.getStorageSync('token'),
        keyValue: this.data.keyValue
      }
    }
    http.request(params, (data) => {
      console.log(data.data.list)
      wx.hideLoading()
      if (!data.data.list[0]) {
        this.setData({
          noData: true
        })
      }
      this.setData({
        courierList: data.data.list
      })
    })
  },

  // 添加/取消快递员
  addCourier: function(e) {
    let item = e.currentTarget.dataset.item;
    let phone = e.currentTarget.dataset.phone;
    let ids = {
      ids:[]
    }
    for (let i = 0; i < wx.getStorageSync('chestList').length; i ++) {
      ids.ids[i] = wx.getStorageSync('chestList')[i].cabinet_id
    }

    if (item.type == '1') {
      // let params = {
      //   url: config.setcourier,
      //   data: {
      //     token: wx.getStorageSync('token'),
      //     phone: item.phone,
      //     'type': '0',
      //     cabId:JSON.stringify(ids)
      //   }
      // }
      // http.request(params, (data) => {
      //   console.log(data)
      //   this.onLoad()
      //   wx.showToast({
      //     title: '添加成功',
      //   })

      // })
      
      wx.navigateTo({
        url: '/pages/priceManage/manage?phone=' + phone,
      })

    } else {
      wx.showModal({
        content: '是否要取消该快递员',
        confirmText: '同意',
        confirmColor: '#EF5D0F',
        success: (res) => {
          if (res.cancel) {

          } else {
            let params = {
              url: config.setcourier,
              data: {
                token: wx.getStorageSync('token'),
                phone: item.phone,
                'type': '1',
                cabId: JSON.stringify(ids)
              }
            }
            http.request(params, (data) => {
              console.log('请求成功'+data)
              this.onShow()
              wx.showToast({
                title: '取消成功',
              })

            })
          }
        }
      })
    }

  },
})