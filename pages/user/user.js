import {
  config
} from '../../utils/config.js'
import {
  HTTP
} from '../../utils/http.js';
const http = new HTTP();
Page({
  data: {
    headImg: '/image/defaultHead.png'
  },
  onLoad:function(options){
    
  },
  onShow:function(){
    let shijianchuo = +wx.getStorageSync('info').create_date * 1000;
    let creatDate = new Date(shijianchuo);
    let creatYear = creatDate.getFullYear();
    let creatMonth = creatDate.getMonth() + 1;
    let creatDay = creatDate.getDate();
    let creatDateCn = `${creatYear}-${creatMonth}-${creatDay}`
    this.setData({
      nickname: wx.getStorageSync('info').nickname,
      phone: wx.getStorageSync('info').phone,
      creatDateCn: creatDateCn
    })
    let params = {
      url: config.getinfo,
      data: {
        token: wx.getStorageSync('token')
      }
    }
    http.request(params,(res)=> {
      this.setData({
        amount: res.data.info,
        isPrimary: res.data.isPrimary
      })
    })
  },
  // 跳转快递员管理
  courierManager: function() {
    wx.navigateTo({
      url: '/pages/courierManage/manage',
    })
  },

  // 跳转收益提现管理
  // courierManager: function () {
  //   wx.navigateTo({
  //     url: 'pages/courierManage/manage',
  //   })
  // },

  // 跳转我的快递柜
  setChest: function() {
    wx.navigateTo({
      url: '/pages/myCabinet/home/home',
    })
  },

  // 跳转收益管理
  moneyRecord: function() {
    wx.navigateTo({
      url: '/pages/vallet/home/home',
    })
  },

  callPhone:function() {
    wx.makePhoneCall({
      phoneNumber: '15768112244' //仅为示例，并非真实的电话号码
    })
  },

  toLogin: function() {
    wx.navigateTo({
      url: '/pages/login/login',
    })
  },

  // 前往商户详情页
  toUserinfo () {
    wx.navigateTo({
      url: '/pages/userInfo/userInfo',
    })
  },

  // 跳转短信
  toSMS () {
    wx.navigateTo({
      url: '/pages/sms/home/home',
    })
  }
})