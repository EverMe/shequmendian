// 鸟巢商家接口常量定义
const config = {
  // baseUrl: 'https://shequmendian.api.tengdev.com/',//根服务器地址
  baseUrl: 'http://shequmendian.api.tengdev.com:9650/',//根服务器地址

  login:'login',//登陆
  regist:'register',//注册
  getsms:'getsms',//验证码
  getcabinetgrid:'getcabinetgrid',//首页用户柜子数据
  getcabinet:'getcabinet',//查询柜机
  searchpackage:'searchpackage',//查询快件
  chestNum:'get/cabinet/count/by/batch/id',//柜子数量
  chestRecord:'get/cabinet/info/by/id',//入柜记录
  searchcabinetlist:'miniprog/searchcabinetlist',//快递搜索
  resendmsg:'resendmsg',//重发短信
  updphone:'updphone',//修改收件人手机
  refreshprice:'refreshprice',//获取价格
  courierlist:'courierlist',//快递员列表
  setcourier:'setcourier',//快递员授权
  updcabname:'updcabname',//修改柜机名称/地址
  getbossincome:'getbossincome',//收益明细
  getexpresscom:'getexpresscom',//快递公司
  getcabinetinfo:'getcabinetinfo',//查询所有柜机信息
  registerv2: 'registerv2',//新注册接口
  loginv2:'loginv2',//新登录接口
  msgcallback: 'msgcallback',//发送短信详情
  // 柜机格口相关
  getrecords: 'expressbox/getrecords',//查询柜机格口投递记录
  sendcommond: 'expressbox/sendcommond',//操作柜机格口
  viewboxlist: 'expresscabinet/viewboxlist',//柜机格口总预览列表
  // 价格设置相关
  getpricelist: 'expresscabinet/getpricelist',//查询柜机价格列表
  getwhitelist: 'expresscabinet/getwhitelist',//查询柜机快递员白名单
  setprice: 'expresscabinet/setprice',//设置柜机价格
  setwhitelistone: 'expresscabinet/setwhitelistone',//设置白名单-单
  setwhitelistall: 'expresscabinet/setwhitelistall',//设置白名单-多
  searchwhitelistall: 'expresscabinet/searchwhitelistall',//搜索快递员-价格已设置柜机列表
  // 逾期时间设置相关
  operatoverduelist: 'expresscabinet/operatoverduelist',//查询柜机逾期时间列表
  setoperatoverdueone: 'expresscabinet/setoperatoverdueone',//柜机投递逾期设置-单
  setoperatoverdueall: 'expresscabinet/setoperatoverdueall',//柜机投递逾期设置-多
  // 预留设置相关
  operatbookinglist: 'expresscabinet/operatbookinglist',//查询柜机格口预留列表
  setoperatbookingamount: 'expresscabinet/setoperatbookingamount',//柜机格口预留设置-数量
  setoperatbookingone: 'expresscabinet/setoperatbookingone',//柜机格口预留设置-快递员-单
  setoperatbookingall: 'expresscabinet/setoperatbookingall',//柜机格口预留设置-快递员-多
  searchbookingall: 'expresscabinet/searchbookingall', //搜索快递员-格口已预留柜机列表
  getreservewhitelist: 'expresscabinet/getreservewhitelist',//查询柜机快递员预留名单
  // 钱包相关
  getbriefamount: 'cabinetboss/getbriefamount',//钱包简介
  getamount: 'cabinetboss/getamount',//钱包余额，短信条数合计
  dayincomelist: 'cabinetboss/dayincomelist',//柜机日收入简介
  dayincomedetail: 'cabinetboss/dayincomedetail',//柜机日收入详情
  orderincomedetail: 'cabinetboss/orderincomedetail',//单票运单收入详情
  bankcardinfo: 'cabinetboss/bankcardinfo',//银行卡信息
  setbankcardinfo: 'cabinetboss/setbankcardinfo',//设置银行卡信息
  withdraw: 'cabinetboss/withdraw',//提现申请
  withdrawhistory: 'cabinetboss/withdrawhistory',//提现记录
  // 拒收管理相关
  getblacklist: 'cabinetcustomerblacklist/getblacklist',//快递柜拒收列表
  setblacklist: 'cabinetcustomerblacklist/setblacklist',//添加快递柜拒收黑名单
  cancelblacklist: 'cabinetcustomerblacklist/cancelblacklist',//删除柜机拒收黑名单
  // 我的快递柜相关
  bindcabinetlist: 'expresscabinet/bindcabinetlist',//查询柜机列表
  bindcabinetdetail: 'expresscabinet/bindcabinetdetail',//查询柜机详情
  setcabinet: 'expresscabinet/setcabinet',//绑定柜机
  cancelcabinet: 'expresscabinet/cancelcabinet',//解绑柜机
  sendmsg: 'expresscabinet/sendmsg',//发送验证码
  // 商家资料
  getinfo: 'cabinetboss/getinfo',//商家资料
  // 短信包
  viewsetmeal: 'msgpackage/viewsetmeal',//短信包套餐列表
  costhistory: 'msgpackage/costhistory',//购买记录
  usedhistory: 'msgpackage/usedhistory',//发送记录
  costbyaccount: 'msgpackage/costbyaccount',//钱包支付
  costbymp: 'msgpackage/costbymp',//微信支付
}

const methods = {
  
}

export{ config }