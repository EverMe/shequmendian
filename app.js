//app.js
import { config } from 'utils/config.js'
import { HTTP } from 'utils/http.js';
const http = new HTTP();
App({
  onLaunch: function () {
    // 版本更新提示
    if (wx.canIUse('getUpdateManager')) {
      const updateManager = wx.getUpdateManager()
      updateManager.onCheckForUpdate(function (res) {
        // 请求完新版本信息的回调
        if (res.hasUpdate) {
          updateManager.onUpdateReady(function () {
            wx.showModal({
              title: '更新提示',
              content: '新版本已经准备好了，是否重启应用更新？',
              success: function (res) {
                if (res.confirm) {
                  // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
                  updateManager.applyUpdate()
                }
              }
            })
          })
          updateManager.onUpdateFailed(function () {
            // 新的版本下载失败
            wx.showModal({
              title: '已经有新版本了哟~',
              content: '新版本已经上线啦~，请您删除当前小程序，重新搜索打开哟~',
            })
          })
        }
      })
    } else {
      // 如果希望用户在最新版本的客户端上体验您的小程序，可以这样子提示
      wx.showModal({
        title: '提示',
        content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
      })
    }
  },
  // 登录
  login: function () {
    wx.login({
      success: res => {
        if (res.code) {
          var params = {
            url: config.login,
            data: {
              jsCode: res.code
            }
          };
          http.request(params,(res) => {
            // console.log(res)
            if (res.data && res.data.info.Phone && res.data.info.Phone!='0') {
              wx.setStorageSync('info', res.data.info)
              wx.setStorageSync('token', res.data.info.token)
            }else {
              wx.setStorageSync('token', res.data.info.token)
              wx.redirectTo({
                url: '/pages/Reg/Reg'
              })
            }
          });
        } else {
          wx.showModal({
            content: '获取用户登录态失败！' + res.errMsg
          })
        }
      }
    });
  },
  globalData: {
    userInfo: null
  }
})